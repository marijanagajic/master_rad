var idPrimTip = "";

function popuniVrstaOsobine(selectObj) {
    var idx = selectObj.selectedIndex;
    var domenId = selectObj.options[idx].value;
    var domenTipString = document.getElementById("domenString").value;
    var domenTipJson = JSON.parse(domenTipString);
    var primTipOsobineString = document.getElementById("primTipOsobineString").value;
    var ptOsobineJson = JSON.parse(primTipOsobineString);
    var comboOsobine = document.getElementById("vrstaOsobine");

    comboOsobine.innerHTML = "";
//    var option = document.createElement('option');
//    option.value = "";
//    option.innerHTML = "";
//    comboOsobine.appendChild(option);

    for (var i in domenTipJson) {
        if (domenTipJson[i].idDomen == domenId) {
            idPrimTip = domenTipJson[i].idPrimitivanTip;
            break;
        }
    }

    for (var i in ptOsobineJson) {
        if (ptOsobineJson[i].idPrimitivanTip == idPrimTip) {
            var osobine = ptOsobineJson[i].osobine;
            var osobineJson = JSON.parse(osobine);
            for (var j in osobineJson) {
                var option = document.createElement('option');
                option.value = osobineJson[j].idVrstaOsobine;
                option.innerHTML = osobineJson[j].nazivVrstaOsobine;
                comboOsobine.appendChild(option);
            }
            break;
        }
    }
}