<%@page import="com.ite.metaregistar.model.TipOrgana"%>
<%@page import="java.util.List"%>
<%@page import="com.ite.metaregistar.model.OgranicenjeVrednosti"%>
<%@page import="com.ite.metaregistar.model.AtributDomena"%>
<%@page import="com.ite.metaregistar.model.EntitetRegistra"%>
<%@page import="com.ite.metaregistar.model.Domen"%>
<%@page import="com.ite.metaregistar.model.RokCuvanjaPodataka"%>
<%@page import="com.ite.metaregistar.model.PravniOsnov"%>
<%@page import="com.ite.metaregistar.model.OsnovNadleznosti"%>
<%@page import="com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra"%>
<%@page import="com.ite.metaregistar.model.Organ"%>
<%@page import="com.ite.metaregistar.model.Registar"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Метарегистар</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="/resursi/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="/resursi/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="/resursi/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <link href="/resursi/home.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="/resursi/css/sb-admin.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <div>  
            <%@include file="top.jsp" %>
            <div id="wrapper">
                <div id="content-wrapper">
                    <div class="container-fluid">
                        <!-- Breadcrumbs-->
                        <ol class="breadcrumb"> <li class="breadcrumb-item active">Додавање корисника и организације</li> 
                        </ol>
                    </div>
                    <%
                        List<Korisnik> korisniciAll = (List<Korisnik>) request.getAttribute("listaKorisnici");
                        List<Organ> organiAll = (List<Organ>) request.getAttribute("listaOrgani");
                        List<NivoPristupa> nivoPristupaAll = (List<NivoPristupa>) request.getAttribute("listaNivoPristupa");
                        List<TipOrgana> tipOrganaAll = (List<TipOrgana>) request.getAttribute("listaTipOrgana");
                        Integer porukaK = (Integer) request.getAttribute("porukaK");
                        Integer porukaO = (Integer) request.getAttribute("porukaO");
                    %>
                    <form method="POST" action="<c:url value="/admin/korisnik/insert"></c:url>">

                            <div style="float: left; width: 50%">
                                <label style="margin-bottom:0px;margin-left: 10%;">Име и презиме </label>
                                <input style="margin-top:0px; width: 55%" type = "text" class="cirilica" name="imePrezime" id="imePrezime" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Корисничко име</label>
                                <input style="margin-top:0px; width: 55%" type = "text"  name="username" id="username" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Лозинка</label>
                                <input style="margin-top:0px; width: 55%;display: -webkit-box;border: 1px solid #3b4e6f;border-radius: 2px 2px 2px 2px;margin-left: 5%;" type = "password" name="password" id="password" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Орган</label>
                                <select id="idOrgan" name="idOrgan" style="width: 55%;" class="selectovan" required="" 
                                        oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                        oninput="setCustomValidity('')">
                                    <option value="">-- Изаберите --</option>
                                <%for (Organ o : organiAll) {%>
                                <option value="<%=o.getIdOrgan()%>"><%=o.getNazivOrgana()%></option>
                                <%}%>  
                            </select>
                            <label style="margin-bottom:0px;margin-left: 10%;">Ниво приступа</label>
                            <select id="idNivoPristupa" name="idNivoPristupa" style="width: 55%;" class="selectovan" required="" 
                                    oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                    oninput="setCustomValidity('')">
                                <option value="">-- Изаберите --</option>
                                <%for (NivoPristupa np : nivoPristupaAll) {%>
                                <option value="<%=np.getIdNivoPristupa()%>"><%=np.getNazivNivoPristupa()%></option>
                                <%}%>  
                            </select>
                            <input style="margin-top: 4%;" type="submit" value="Сачувај корисника"/>

                        </div>
                    </form>
                    <form method="POST"  action="<c:url value="/admin/organ/insert"></c:url>">
                            <div style="float: left;width: 50%">
                                <label style="margin-bottom:0px;margin-left: 10%;">Назив органа </label>
                                <input style="margin-top:0px; width: 55%" type = "text" class="cirilica" name="nazivOrgana" id="nazivOrgana" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Пиб</label>
                                <input style="margin-top:0px; width: 55%" type = "text"  name="pib" id="pib" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Матични број</label>
                                <input style="margin-top:0px; width: 55%" type = "text" name="mb" id="mb" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Тип органа</label>
                                <select id="idTipOrgana" name="idTipOrgana" style="width: 55%;" class="selectovan" required="" 
                                        oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                        oninput="setCustomValidity('')">
                                    <option value="">-- Изаберите --</option>
                                <%for (TipOrgana to : tipOrganaAll) {%>
                                <option value="<%=to.getIdTipOrgana()%>"><%=to.getNazivTipOrgana()%></option>
                                <%}%>  
                            </select>

                            <input style="margin-top: 4%;" type="submit" value="Сачувај орган"/>
                        </div>
                    </form>
                </div>
                <!-- /.content-wrapper -->
            </div>
            <!-- /.wrapper -->
        </div>
        <%@include  file="bottom.jsp"%>
        <%@include file="logout.jsp" %>
        <script type="text/javascript">
            var porukaK = <%=porukaK%>;
            var porukaO = <%=porukaO%>;
            if (porukaK == 1) {
            alert("Успешно сачуван корисник!");
            }else if(porukaK == 2){
            alert("Неуспешно чување корисника! Молимо Вас покушајте поново!");
            }
            if (porukaO == 1) {
            alert("Успешно сачуван орган!");
            }else if(porukaO == 2){
            alert("Неуспешно чување органа! Молимо Вас покушајте поново!");
            }

            $(".cirilica").keypress(function (e) {
                var verified = String.fromCharCode(e.which).match(/[a-zA-ZžŽšŠćĆčČđĐ]/);
                if (verified) {
                    e.preventDefault();
                    alert("Молимо Вас користитет ћирилично писмо!");
                }
            });
            $('.cirilica').on('paste input propertychange', function (e) {
                $(this).val($(this).val().replace(/[a-zA-ZžŽšŠćĆčČđĐ]/g, ''));
            });
        </script>
        <script src="/resursi/vendor/jquery/jquery.min.js"></script>
        <script src="/resursi/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/resursi/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="/resursi/vendor/chart.js/Chart.min.js"></script>
        <script src="/resursi/vendor/datatables/jquery.dataTables.js"></script>
        <script src="/resursi/vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/resursi/js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="/resursi/js/demo/datatables-demo.js"></script>
        <script src="/resursi/js/demo/chart-area-demo.js"></script>
    </body>
</html>