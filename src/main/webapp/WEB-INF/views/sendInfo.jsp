<%@page import="com.ite.metaregistar.model.TipOrgana"%>
<%@page import="java.util.List"%>
<%@page import="com.ite.metaregistar.model.OgranicenjeVrednosti"%>
<%@page import="com.ite.metaregistar.model.AtributDomena"%>
<%@page import="com.ite.metaregistar.model.EntitetRegistra"%>
<%@page import="com.ite.metaregistar.model.Domen"%>
<%@page import="com.ite.metaregistar.model.RokCuvanjaPodataka"%>
<%@page import="com.ite.metaregistar.model.PravniOsnov"%>
<%@page import="com.ite.metaregistar.model.OsnovNadleznosti"%>
<%@page import="com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra"%>
<%@page import="com.ite.metaregistar.model.Organ"%>
<%@page import="com.ite.metaregistar.model.Registar"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Метарегистар</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="/resursi/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="/resursi/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="/resursi/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <link href="/resursi/home.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="/resursi/css/sb-admin.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <div>  
            <%@include file="top.jsp" %>
            <div id="wrapper">
                <div id="content-wrapper">
                    <div class="container-fluid">
                        <!-- Breadcrumbs-->
                        <ol class="breadcrumb"> <li class="breadcrumb-item active">Обавештавање о промени изворног податка</li> 
                        </ol>
                    </div>
                    <%                        List<Domen> poslovniDomenAll = (List<Domen>) request.getAttribute("poslovniDomenLista");
                        String poruka = (String) request.getAttribute("poruka");
                    %>
                    <form method="POST" action="<c:url value="/user/sendInfo"></c:url>">

                            <div style="float: left; width: 50%">
                                <label style="margin-bottom:0px;margin-left: 10%;">Нова вредност </label>
                                <input style="margin-top:0px; width: 55%" type = "text" class="cirilica" name="novaVrednost" id="novaVrednost" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Идентификациона ознака</label>
                                <input style="margin-top:0px; width: 55%" type = "text"  name="identO" id="identO" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Вредност идентификационе ознаке</label>
                                <input style="margin-top:0px; width: 55%;display: -webkit-box;border: 1px solid #3b4e6f;border-radius: 2px 2px 2px 2px;margin-left: 5%;" type = "text" name="vrednostIdentO" id="vrednostIdentO" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                       oninput="setCustomValidity('')"/>
                                <label style="margin-bottom:0px;margin-left: 10%;">Пословни домен</label>
                                <select id="idDomen" name="idDomen" style="width: 55%;" class="selectovan" required="" 
                                        oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                        oninput="setCustomValidity('')">
                                    <option value="">-- Изаберите --</option>
                                <%for (Domen d : poslovniDomenAll) {%>
                                <option value="<%=d.getIdDomen()%>"><%=d.getNazivDomen()%></option>
                                <%}%>  
                            </select>
                            <input style="margin-top: 4%;" type="submit" value="Пошаљи обавештење"/>

                        </div>
                    </form>

                </div>
                <!-- /.content-wrapper -->
            </div>
            <!-- /.wrapper -->
        </div>
        <%@include  file="bottom.jsp"%>
        <%@include file="logout.jsp" %>
        <script type="text/javascript">
            var poruka = "<%=poruka%>";
            if (poruka != "null") {
                alert(poruka);
            }

        </script>
        <script src="/resursi/vendor/jquery/jquery.min.js"></script>
        <script src="/resursi/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/resursi/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="/resursi/vendor/chart.js/Chart.min.js"></script>
        <script src="/resursi/vendor/datatables/jquery.dataTables.js"></script>
        <script src="/resursi/vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/resursi/js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="/resursi/js/demo/datatables-demo.js"></script>
        <script src="/resursi/js/demo/chart-area-demo.js"></script>
    </body>
</html>