<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page language="java" import="java.util.*" contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="/resursi/style.css" rel="stylesheet"> 
        <title>Метарегистар</title>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">

                <div class="fadeIn first">
                    <h2 style="margin-bottom: 10%; color: #004085">Метарегистар</h2>
                </div>

                <form id="sign_in" method="POST" action="<c:url value="/login"></c:url>">


                        <input type="text" name="username" class="fadeIn second" placeholder="Корисничко име" >


                        <input type="password" name="password" class="fadeIn third" placeholder="Лозинка" >


<!--  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> -->

                    <input type = "submit" class="fadeIn fourth" value ="ПРИЈАВИ СЕ" />


                </form>
            </div>
        </div>
    </body>
</html>
