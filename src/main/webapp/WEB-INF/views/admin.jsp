<%@page import="com.ite.metaregistar.model.Korisnik"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ite.metaregistar.model.NivoPristupa"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Метарегистар</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="/resursi/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="/resursi/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="/resursi/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <link href="/resursi/home.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="/resursi/css/sb-admin.css" rel="stylesheet">
    </head>
    <body id="page-top">


        <%
            ArrayList<NivoPristupa> np = (ArrayList<NivoPristupa>) request.getAttribute("listaPristupa");
        %>

        <%@include file="top.jsp" %>
        <div>  
            <div id="wrapper">

                <!-- Sidebar -->
                <%@include file="leftSideAdmin.jsp"%>

                <div id="content-wrapper">

                    <div class="container-fluid">

                        <!-- Breadcrumbs-->
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Администрација</a>
                            </li>
                            <li class="breadcrumb-item active">Корисник</li>
                            </li>
                            <li class="breadcrumb-item active">Додај корисника</li>
                        </ol>

                        <!-- Page Content -->
                        <h5 class="copyright text-center my-auto">&nbsp;Унос новoг корисника</h5> 
                        <hr>
                        <c:if test="${not empty obavestenje}">
                            <div class="alert alert-${obavestenje} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">x</span>   
                                </button>
                                <strong>${obavestenje}</strong>
                            </div>
                        </c:if>
                        <div id="formContent">   
                            <form id="sign_in"  method="POST" action="<c:url value="/admin/dodajKorisnika"></c:url>">

                                    <h5  style="width:20%"><label>Име корисника</label></h5>
                                    <input style="width:20%" class="cirilica" type="text" required name="imeKorisnik" class="fadeIn second" placeholder="Име" 
                                           oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                           oninput="setCustomValidity('')"/>
                                    <h5  style="width:20%"><label>Презиме корисника</label></h5>
                                    <input style="width:20%" class="cirilica" type="text" required name="prezimeKorisnik" class="fadeIn second" placeholder="Презиме" 
                                           oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                           oninput="setCustomValidity('')"/>

                                    <h5  style="width:20%"><label>Статус</label></h5>
                                    <select style="width:20%" class="selectovan" name="status">
                                        <option value="1">Активан</option>
                                        <option value="0">Неактиван</option>
                                    </select>
                                    <h5  style="width:20%"><label>Телефон</label></h5>
                                    <input style="width:20%" type="text" pattern="0[0-9]{7,9}" required name="telefon" class="fadeIn second" placeholder="Телефон"
                                           oninvalid="this.setCustomValidity('Oбавезно поље! Телефон мора бити у формату 0XXXXXXXXX')" 
                                           oninput="setCustomValidity('')"/>
                                    <h5  style="width:20%"><label>Адреса електронске поште</label></h5>
                                    <input style="width:20%" type="text" required name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="fadeIn second" placeholder="формат: primer@gov.rs" 
                                           oninvalid="this.setCustomValidity('Адреса електронске поште није правилно унета!')" 
                                           oninput="setCustomValidity('')"/>
                                    <h5  style="width:20%"><label>Корисничко име</label></h5>
                                    <input style="width:20%" type="text" required name="username" class="fadeIn second" placeholder="Корисничко име" 
                                           oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                           oninput="setCustomValidity('')">
                                    <h5  style="width:20%"><label>Лозинка</label></h5>
                                    <input style="width:20%" type="password" required name="password" pattern="^(?=.*[а-яa-zћђљњџ])(?=.*[А-ЯA-ZЏЋЂЉЊЏ])(?=.*\d)(?=.*[#$^+.=!*()@%&]).{6,}$" class="fadeIn second" placeholder="Лозинка" 
                                           oninvalid="this.setCustomValidity('Лозинка мора садржати најмање један број, једно велико и мало слово и најмање 6 знакова!!')" 
                                           oninput="setCustomValidity('')">
                                    <h5  style="width:20%"><label>Организација</label></h5>
                                    <select style="width:20%" class="selectovan" required name="organizacija"
                                            oninvalid="this.setCustomValidity('Изаберите организацију!')" 
                                            oninput="setCustomValidity('')">
                                        <option value="">Организација:</option>
                                              
                                </select>
                                <h5  style="width:20%"><label>Ниво приступа</label></h5>
                                <select style="width:20%" class="selectovan" required name="nazivPristupa"
                                        oninvalid="this.setCustomValidity('Изаберите ниво приступа!')" 
                                        oninput="setCustomValidity('')">
                                    <option class="selectovan" value="">Ниво приступа:</option>
                                   
                                </select>
                                </br></br></br>
                                <input type = "submit" class="container my-auto" style="width: 30%;margin-left: 38%" value ="Додај kорисника"/></td></br></br>

                            </form>
                        </div>
                    </div>

                    <%
                        String msg = (String) request.getAttribute("obavestenje");
                    %>
                    <%
                        if (msg == null) {
                            msg = "";
                    %>
                    <%} else {%>
                    <div>
                        <h4><%=(String) request.getAttribute("obavestenje")%></h4>
                    </div>
                    <%}%>


                </div>
                <!-- /.content-wrapper -->

            </div>
            <%@include  file="bottom.jsp"%>

        </div>
        <%@include file="logout.jsp" %>
        
        <script type="text/javascript">
            $(".cirilica").keypress(function (e) {
                var verified = String.fromCharCode(e.which).match(/[a-zA-ZžŽšŠćĆčČđĐ]/);
                if (verified) {
                    e.preventDefault();
                    alert("Молимо Вас користите ћирилична слова!");
                }
            });
            $('.cirilica').on('paste input propertychange', function (e) {
                $(this).val($(this).val().replace(/[a-zA-ZžŽšŠćĆčČđĐ]/g, ''));
            });

        </script>
        <script src="/resursi/vendor/jquery/jquery.min.js"></script>
        <script src="/resursi/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/resursi/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="/resursi/vendor/chart.js/Chart.min.js"></script>
        <script src="/resursi/vendor/datatables/jquery.dataTables.js"></script>
        <script src="/resursi/vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/resursi/js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="/resursi/js/demo/datatables-demo.js"></script>
        <script src="/resursi/js/demo/chart-area-demo.js"></script>
    </body>
</html>

