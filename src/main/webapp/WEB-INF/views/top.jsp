<%@page import="com.ite.metaregistar.model.NivoPristupa"%>
<%@page import="com.ite.metaregistar.model.Korisnik"%>
<%@page language="java" import="java.util.*" contentType="text/html" pageEncoding="UTF-8"%>   
<%
    Korisnik k = (Korisnik) session.getAttribute("korisnik");
    boolean admin = false;
    for (NivoPristupa np : k.getNivoPristupaCollection()) {
        if (np.getNazivNivoPristupa().equals("ROLE_ADMIN")) {
            admin = true;
        }
    }
%>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
    <a class="navbar-brand mr-1" href="/user/">МЕТАРЕГИСТАР</a>
    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <%if (admin) {%>
        <li class="nav-item">
            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i style="font-style: initial">Администрација</i>
                <span class="badge badge-danger"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
            </div>

            
        </li>
        <%} else {%>
        <li class="nav-item">
            <a class="nav-link" href="/user/promena">
                <span>Промена вредности ентитета</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/user/registar/">
                <span>Нови регистар</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/user/">
                <span>Преглед регистри</span></a>
        </li>
        <%}%>

    </ul>
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
        </div>
    </form>

    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Одјави се</a>
            </div>
        </li>
    </ul>
</nav>