<%@page import="com.ite.metaregistar.model.AtributDomena"%>
<%@page import="com.ite.metaregistar.model.OgranicenjeVrednosti"%>
<%@page import="com.ite.metaregistar.model.EntitetRegistra"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ite.metaregistar.model.Domen"%>
<%@page import="com.ite.metaregistar.model.RokCuvanjaPodataka"%>
<%@page import="com.ite.metaregistar.model.PravniOsnov"%>
<%@page import="com.ite.metaregistar.model.OsnovNadleznosti"%>
<%@page import="com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra"%>
<%@page import="com.ite.metaregistar.model.Organ"%>
<%@page import="com.ite.metaregistar.model.Registar"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
<%@page language="java" import="java.util.*" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Метарегистар</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="/resursi/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="/resursi/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="/resursi/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <link href="/resursi/home.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="/resursi/css/sb-admin.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <div>  
            <%@include file="top.jsp" %>
            <div id="wrapper">
                <div id="content-wrapper">
                    <div class="container-fluid">
                        <!-- Breadcrumbs-->
                        <ol class="breadcrumb"> <li class="breadcrumb-item active">Подаци о регистру</li> 
                        </ol>
                    </div>
                    <%                        Registar registar = (Registar) request.getAttribute("registar");
                        List<FrekventnostAzuriranjaRegistra> frekventnostARAll = (List<FrekventnostAzuriranjaRegistra>) request.getAttribute("listaFrekventnostAzuriranja");
                        List<OsnovNadleznosti> osnovNAdleznostiAll = (List<OsnovNadleznosti>) request.getAttribute("listaOsnovNadleznosti");
                        List<PravniOsnov> pravniOsnovAll = (List<PravniOsnov>) request.getAttribute("listPravniOsnov");
                        List<RokCuvanjaPodataka> rokCuvanjaPodatakaAll = (List<RokCuvanjaPodataka>) request.getAttribute("listRokCuvanjaPodataka");
                        List<Domen> domenAll = (List<Domen>) request.getAttribute("listaDomen");
                        List<String> naziviEntiteta = (List<String>) request.getAttribute("naziviEntiteta");
                        Korisnik korisnik = (Korisnik) session.getAttribute("korisnik");
                        String entitetiJson = (String) request.getAttribute("entitetiJson");
                        String primTipOsobineJson = (String) request.getAttribute("listaPrimTipOsobine");
                        String domenTipJson = (String) request.getAttribute("domenTipJson");
                        Integer poruka = (Integer) request.getAttribute("poruka");
                    %>
                    <input type="hidden" id="entitetiString" value='<%=entitetiJson%>'/>
                    <input type="hidden" id="domenString" value='<%=domenTipJson%>'/>
                    <input type="hidden" id="primTipOsobineString" value='<%=primTipOsobineJson%>'/>

                    <form method="POST" action="<c:url value="/user/registar/update"></c:url>">
                        <input type="hidden" value="<%=registar.getIdRegistar()%>" name="idRegistar">
                        <div style="float: left; width: 30%">
                            <label style="margin-bottom:0px;margin-left: 10%;">Назив регистра</label>
                            <input style="margin-top:0px; width: 80%" type = "text" class="cirilica" name="nazivRegistra" id="nazivRegistra" value="<%=registar.getNazivRegistar()%>" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                   oninput="setCustomValidity('')"/>
                            <label style="margin-bottom:0px;margin-left: 10%;">Надлежни орган</label>
                            <input style="margin-top:0px; width: 80%" type = "text" name="nadlezniOrgan" id="nadlezniOrgan" value="<%=registar.getIdOrgan().getNazivOrgana()%>" readonly/>
                            <label style="margin-bottom:0px;margin-left: 10%;">URL сервиса обавештења о промени</label>
                            <input style="margin-top:0px; width: 80%" type = "text" name="url" id="url"  value="<%=registar.getUrl()%>" required oninvalid="this.setCustomValidity('Обавезно поље!')" 
                                   oninput="setCustomValidity('')"/>

                            <label style="margin-bottom:0px;margin-left: 10%;">Датум успостављања регистра</label>
                            <input style="margin-top:0px; width: 55%" type = "date"  name="datumUspostavljanja" id="datumUspostavljanja" required oninvalid="this.setCustomValidity('Обавезно поље! Датум мора бити ранији од данашњег!')" 
                                   oninput="setCustomValidity('')"/>
                            <label style="margin-bottom:0px;margin-left: 10%;">Фреквентност ажурирања регистра</label>
                            <select id="frekventnostAR" name="frekventnostAR" style="width: 55%;" class="selectovan" required="" 
                                    oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                    oninput="setCustomValidity('')">
                                <option value="">-- Изаберите --</option>
                                <%for (FrekventnostAzuriranjaRegistra far : frekventnostARAll) {%>
                                <option <%=(String.valueOf(far.getIdFrekventnostAzuriranjaRegistra()).equals(String.valueOf(registar.getIdFrekventnostAzuriranjaRegistra().getIdFrekventnostAzuriranjaRegistra())) ? "selected" : "")%>
                                    value="<%=far.getIdFrekventnostAzuriranjaRegistra()%>"><%=far.getNazivFrekventnostAzuriranjaRegistra()%></option>
                                <%}%>  
                            </select>
                            <label style="margin-bottom:0px;margin-left: 10%;">Надлежност</label>
                            <select id="osnovNadleznosti" name="osnovNadleznosti" style="width: 55%;" class="selectovan" required="" 
                                    oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                    oninput="setCustomValidity('')">
                                <option value="">-- Изаберите --</option>
                                <%for (OsnovNadleznosti on : osnovNAdleznostiAll) {%>
                                <option <%=(String.valueOf(on.getIdNadleznost()).equals(String.valueOf(registar.getIdOsnovNadleznosti().getIdNadleznost())) ? "selected" : "")%> 
                                    value="<%=on.getIdNadleznost()%>"><%=on.getNazivNadleznost()%></option>
                                <%}%>  
                            </select>
                            <div>
                                <input type="hidden" id="vazeci" name="vazeci" value="<%=((registar.getIdPravniOsnov() != null) ? 1 : 0)%>" />
                                <input style="margin-left: 10%;" type="checkbox" onclick="vazeciCheckBox();" id="checkVazeci" <%=((registar.getIdPravniOsnov() != null) ? "checked" : "")%>/><label>Правни основ</label>
                                <input style="margin-left: 4%;" type="checkbox" onclick="nevazeciCheckBox();" id="checkNevazeci" <%=((registar.getIdPravniOsnov() == null) ? "checked" : "")%>/><label>Други основ</label>
                            </div>
                            <select id="pravniOsnov" name="pravniOsnov" style="<%=((registar.getIdPravniOsnov() != null) ? "width: 55%;" : "width:55%;display:none;")%>" class="selectovan">
                                <option value="-1">-- Изаберите --</option>
                                <%for (PravniOsnov po : pravniOsnovAll) {%>
                                <option value="<%=po.getIdPravniOsnov()%>" 
                                        <%=((registar.getIdPravniOsnov() != null) ? ((String.valueOf(po.getIdPravniOsnov()).equals(String.valueOf(registar.getIdPravniOsnov().getIdPravniOsnov()))) ? "selected" : "") : "")%>  ><%=po.getNazivPravniOsnov()%></option>
                                <%}%>  
                            </select>
                            <input style="<%=((registar.getIdPravniOsnov() != null) ? "margin-top:0px; width: 80%; display:none;" : "margin-top:0px; width: 80%;")%>" type = "text" class="cirilica" name="propisNevazeci" id="propisNevazeci" value="<%=((registar.getPropisNevazeci() == null) ? "" : registar.getPropisNevazeci())%>" placeholder="Назив закона: број из Службеног Гласника"/>
                            <label style="margin-bottom:0px;margin-left: 10%;">Рок чувања података</label>
                            <select id="rokCuvanjPodataka" name="rokCuvanjPodataka" style="width: 55%;" class="selectovan" required="" 
                                    oninvalid="this.setCustomValidity('Oбавезно поље!')" 
                                    oninput="setCustomValidity('')">
                                <option value="">-- Изаберите --</option>
                                <%for (RokCuvanjaPodataka rcp : rokCuvanjaPodatakaAll) {%>
                                <option <%=(String.valueOf(rcp.getIdRokCuvanjaPodataka()).equals(String.valueOf(registar.getIdRokCuvanjaPodataka().getIdRokCuvanjaPodataka())) ? "selected" : "")%> 
                                    value="<%=rcp.getIdRokCuvanjaPodataka()%>"><%=rcp.getNazivRokCuvanjaPodataka()%></option>
                                <%}%>  
                            </select>
                        </div>
                        <div style="float: left;width: 70%">
                            <input style="margin-left: 4%" name="checkElektronski" type="checkbox" <%=(registar.getUElektronskomObliku() ? "checked" : "")%> value="1" />
                            <input type="hidden" name="checkElektronski" value="0">
                            <label>Регистар се води у електронском облику?</label>
                            <label style="margin-bottom:0px;margin-left: 5%;display: -webkit-box;">Напомена</label>
                            <textarea class="textAreaCustom cirilica" id="napomenaReg"  name="napomena"><%=registar.getNapomena()%></textarea>

                            <div class="entitet-blok">
                                <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">
                                    <!-- Breadcrumbs-->
                                    <ol class="breadcrumb"> <li class="breadcrumb-item active"><b>Подаци регистра</b></li> 
                                    </ol>
                                </div>

                                <input type="hidden" id="nizNazivi" name="nizNazivi" value="">
                                <input type="hidden" id="podaciZaInsertJson" name="podaciZaInsertJson" value="">
                                <div style="width: 100%; float:left">
                                    <div style="width: 50%; float:left">
                                        <label style="margin-bottom:0px;margin-left: 10%;">Назив податка</label>
                                        <input style="margin-top:0px; width: 90%;" type = "text"  name="nazivPodatka" id="nazivPodatka" />
                                    </div>
                                    <div style="width: 50%; float:left">
                                        <label style="margin-bottom:0px;margin-left: 10%;">Назив табеле</label>
                                        <input style="margin-top:0px; width: 90%;" type = "text"  name="nazivTabele" id="nazivTabele" />
                                    </div>
                                </div>
                                <div >
                                    <input id="checkIzvorni" style="margin-left: 3%" type="checkbox" onclick="checkIzvorniP()"/>
                                    <label style="margin-bottom:2%;">Изворни податак</label>
                                </div>
                                <div id="poslovniDomenIzvorniP" style="display:none">
                                    <div style="width: 50%; float:left">
                                        <div>
                                            <div style="width: 50%; float:left">
                                                <label style="margin-bottom:0px;margin-left: 18%;white-space: pre-wrap;">Назив пословног домена</label>
                                                <input style="margin-top:0px; width: 90%;" type = "text" class="cirilica" name="nazivPoslovnogDomena" id="nazivPoslovnogDomena" />
                                            </div>
                                            <div style="width: 50%; float:left">
                                                <label style="margin-bottom:0px;margin-left: 18%;white-space: pre-wrap;">Семантички домен &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <select id="domenSemanticki" name="domenSemanticki" style="width: 92%;" onchange="popuniVrstaOsobine(this);"  class="selectovan">
                                                    <option id="defaultComboSem" value="">-- Изаберите --</option>
                                                    <optgroup label="Семантички домен">
                                                        <%for (Domen domen : domenAll) {
                                                                String title = "";
                                                                String primTip = "";
                                                                if (domen.getIdTipDomena().getIdTipDomena() == 1) {
                                                                    if (domen.getAtributDomenaCollection() != null) {
                                                                        for (AtributDomena ad : domen.getAtributDomenaCollection()) {
                                                                            List<OgranicenjeVrednosti> l = (List<OgranicenjeVrednosti>) ad.getOgranicenjeVrednostiCollection();
                                                                            if (ad.getIdTipAtributDomena().getIdTipAtributDomena() == 1) {
                                                                                primTip = ad.getIdPrimitivanTip().getNazivPrimitivanTip();
                                                                            }
                                                                            title += " " + ad.getNazivAtributDomena() + ": " + ad.getIdPrimitivanTip().getNazivPrimitivanTip();
                                                                            if (l.size() != 0) {
                                                                                for (OgranicenjeVrednosti ov : l) {
                                                                                    title += " (" + ov.getVrstaOsobine().getNazivVrstaOsobine() + ": " + ov.getVrednostOsobine() + ")";
                                                                                }
                                                                            }
                                                                            title += "\n";
                                                                        }
                                                                    }
                                                        %>
                                                        <option title="<%=title%>" value="<%=domen.getIdDomen()%>"><%=domen.getNazivDomen()%></option>
                                                        <%}
                                                            }%>  
                                                    </optgroup>

                                                </select>
                                            </div>
                                        </div>
                                        <div style="display: inline-block;">
                                            <div style="width: 33%; float:left">
                                                <label style="margin-bottom:0px;margin-left: 23%;">Врста&nbsp; ограничења</label>
                                                <select id="vrstaOsobine" style="width:85%;" name="vrstaOsobine"  class="selectovan">

                                                </select>
                                            </div>
                                            <div style="width: 33%; float:left">
                                                <label style="margin-bottom:0px;margin-left: 23%;">Вредност ограничења</label>
                                                <input style="margin-top:1%;width:80%;" type = "text" class="cirilica" name="ogranicenjeVrednosti" id="ogranicenjeVrednosti"/>
                                            </div>
                                            <div style="width:33%;float: left;">
                                                <button style="margin-top: 28%;background-color: tomato;margin-left:21%; margin-bottom: 20px;" type="button" class="btn btn-primary" onclick="dodajOgranicenje()" >Додај ограничење</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive" style="width: 30%; float:left; margin-left: 10%">
                                        <table id="tabelaOgranicenja" class="table table-bordered" width="80%" cellspacing="0">
                                            <thead>
                                            <th>Врста ограничења</th>
                                            <th>Вредност ограничења</th>
                                            </thead>
                                            <tbody id="tbodyOgranicenja">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="domenNijeIzvorniP" style="display:block">
                                    <div style="width: 33%; float:left">
                                        <label style="margin-bottom:0px;margin-left: 18%;">Домен</label>
                                        <select id="domen" name="domen" style="width: 90%;" class="selectovan">
                                            <option id="defaultCombo" value="">-- Изаберите --</option>
                                           
                                            <optgroup label="Пословни домен">
                                                <%for (Domen domen : domenAll) {
                                                        String title = "";
                                                        if (domen.getIdTipDomena().getIdTipDomena() == 2) {
                                                            if (domen.getAtributDomenaCollection() != null) {
                                                                for (AtributDomena ad : domen.getAtributDomenaCollection()) {
                                                                    List<OgranicenjeVrednosti> l = (List<OgranicenjeVrednosti>) ad.getOgranicenjeVrednostiCollection();
                                                                    title += " " + ad.getNazivAtributDomena() + ": " + ad.getIdPrimitivanTip().getNazivPrimitivanTip();
                                                                    if (l.size() != 0) {
                                                                        for (OgranicenjeVrednosti ov : l) {
                                                                            title += " (" + ov.getVrstaOsobine().getNazivVrstaOsobine() + ": " + ov.getVrednostOsobine() + ")";
                                                                        }
                                                                    }
                                                                    title += "\n";
                                                                }
                                                            }
                                                %>
                                                <option title="<%=title%>" value="<%=domen.getIdDomen()%>"><%=domen.getNazivDomen()%></option>
                                                <%}
                                                    }%>  
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div style="display:none">
                                        <div style="width: 40%; float: left">
                                        </div>
                                        <div style="width: 40%; float: left">
                                        </div>
                                    </div>
                                </div>
                                <div style="float:left;margin-right: 45%;">
                                    <label style="margin-bottom:0px;margin-left: 10%;">Напомена</label>
                                    <textarea style="margin-bottom:5%;width: 450px;margin-left: 7%;" class="textAreaCustom cirilica" id="napomena" > </textarea>
                                </div>


                                <button style="margin-left:40%; width: 20%; margin-bottom: 3%;margin-right: 40%;" type="button" class="btn btn-primary" onclick="popuniPodatak()" >Додај</button>
                                <div class="card mb-3" style="width: 90%; margin-left: 5%">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tabelaPodaci" class="table table-bordered" width="100%" cellspacing="0">
                                                <thead>
                                                <th>Назив податка у регистру</th>
                                                <th>Домен</th>
                                                <th>Обриши</th>
                                                </thead>
                                                <tbody id="tbodyPodaci">
                                                    <%for (EntitetRegistra er : registar.getEntitetRegistraCollection()) {
                                                    %>
                                                    <tr>
                                                        <td><%=er.getNazivEntitetRegistra()%></td>
                                                        <td><%=er.getIdDomen().getNazivDomen()%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" value="Измени" style="margin-top: 40px;" />
                        </div>
                    </form>
                </div>
                <!-- /.content-wrapper -->
            </div>

        </div>
        <%@include  file="bottom.jsp"%>
        <%@include file="logout.jsp" %>
        <script type="text/javascript">

            var brojEntitetaStart = <%=registar.getEntitetRegistraCollection().size()%>;
            var podaciListaJson = [];
            var ogranicenjaPoslovniDomenJson = [];


            function nevazeciCheckBox() {
                var propisN = document.getElementById("propisNevazeci");
                var pravniOsnov = document.getElementById("pravniOsnov");
                var checkN = document.getElementById("checkNevazeci");
                var checkV = document.getElementById("checkVazeci");
                var vazeciHidden = document.getElementById("vazeci");
                if (checkN.checked == true) {
                    propisN.style.display = "block";
                    checkV.checked = false;
                    pravniOsnov.style.display = "none";
                    propisN.setAttribute("required", "");
                    propisN.setAttribute("oninvalid", "this.setCustomValidity('Обавезно поље!')");
                    propisN.setAttribute("oninput", "setCustomValidity('')");
                    pravniOsnov.removeAttribute("required");
                    pravniOsnov.removeAttribute("oninvalid");
                    pravniOsnov.removeAttribute("oninput");
                    vazeciHidden.value = "0";
                } else {
                    checkV.checked = true;
                    propisN.style.display = "none";
                    pravniOsnov.style.display = "block";
                    pravniOsnov.setAttribute("required", "");
                    pravniOsnov.setAttribute("oninvalid", "this.setCustomValidity('Обавезно поље!')");
                    pravniOsnov.setAttribute("oninput", "setCustomValidity('')");
                    propisN.removeAttribute("required");
                    propisN.removeAttribute("oninvalid");
                    propisN.removeAttribute("oninput");
                    vazeciHidden.value = "1";
                }
            }

            function vazeciCheckBox() {
                var propisN = document.getElementById("propisNevazeci");
                var pravniOsnov = document.getElementById("pravniOsnov");
                var checkN = document.getElementById("checkNevazeci");
                var checkV = document.getElementById("checkVazeci");
                var vazeciHidden = document.getElementById("vazeci");
                if (checkV.checked == true) {
                    propisN.style.display = "none";
                    checkN.checked = false;
                    pravniOsnov.style.display = "block";
                    pravniOsnov.setAttribute("required", "");
                    pravniOsnov.setAttribute("oninvalid", "this.setCustomValidity('Обавезно поље!')");
                    pravniOsnov.setAttribute("oninput", "setCustomValidity('')");
                    propisN.removeAttribute("required");
                    propisN.removeAttribute("oninvalid");
                    propisN.removeAttribute("oninput");
                    vazeciHidden.value = "1";
                } else {
                    checkN.checked = true;
                    propisN.style.display = "block";
                    pravniOsnov.style.display = "none";
                    propisN.setAttribute("required", "");
                    propisN.setAttribute("oninvalid", "this.setCustomValidity('Обавезно поље!')");
                    propisN.setAttribute("oninput", "setCustomValidity('')");
                    pravniOsnov.removeAttribute("required");
                    pravniOsnov.removeAttribute("oninvalid");
                    pravniOsnov.removeAttribute("oninput");
                    vazeciHidden.value = "0";
                }
            }

            var datum = document.getElementById("datumUspostavljanja");
            var utc = new Date().toJSON().slice(0, 10);
            datum.setAttribute("max", utc);
            var datumUsp = "<%=registar.getDatumUspostavljanja()%>";
            datum.setAttribute("value", datumUsp.substring(0, 10));
            $(".cirilica").keypress(function (e) {
                var verified = String.fromCharCode(e.which).match(/[a-zA-ZžŽšŠćĆčČđĐ]/);
                if (verified) {
                    e.preventDefault();
                    alert("Молимо Вас користите ћирилична слова!");
                }
            });
            $('.cirilica').on('paste input propertychange', function (e) {
                $(this).val($(this).val().replace(/[a-zA-ZžŽšŠćĆčČđĐ]/g, ''));
            });
            var postojeciNazivi = "<%=naziviEntiteta%>";
            var niz = postojeciNazivi.substring(1, postojeciNazivi.length - 1);
            var nizPostojecih = niz.split(",");
            var nizPostojecihNaziva = [];
            for (var item in nizPostojecih) {
                nizPostojecihNaziva.push(nizPostojecih[item].trim());
            }
            var nizNazivPodataka = [];

            function popuniPodatak() {
                var nazivPodatak = document.getElementById("nazivPodatka").value;
                var nazivTabele = document.getElementById("nazivTabele").value;
                var napomena = document.getElementById("napomena").value;
                var nazivPoslovniDomen = document.getElementById("nazivPoslovnogDomena").value;
                var domenSemanticki = document.getElementById("domenSemanticki");
                var domenSemantickiID = domenSemanticki.options[domenSemanticki.selectedIndex].value;
                var izvorni = document.getElementById("checkIzvorni").checked;
                var idD = document.getElementById("domen");
                var domen = idD.options[idD.selectedIndex].text;
                var idDomen = idD.options[idD.selectedIndex].value;
                var comboDefaultValue = document.getElementById("defaultCombo").text;
                var comboDefaultValueSemD = document.getElementById("defaultComboSem").text;
                if (nazivPodatak == "") {
                    alert("Морате унети назив податка!");
                } else if (nazivTabele == "") {
                    alert("Морате унети назив табеле!");
                } else if (izvorni == false && domen == comboDefaultValue) {
                    alert("Морате изаврати домен податка!");
                } else if (izvorni == true && domenSemanticki.options[domenSemanticki.selectedIndex].text == comboDefaultValueSemD) {
                    alert("Морате изаврати семантички домен над којим се дефинише пословни домен!");
                } else if (izvorni == true && nazivPoslovniDomen == "") {
                    alert("Морате унети назив пословног домена!");
                } else if (nizNazivPodataka.indexOf(nazivPodatak) == -1 && nizPostojecihNaziva.indexOf(nazivPodatak) == -1) {
                    nizNazivPodataka.push(nazivPodatak);

                    var podatakJson = {nazivEntitet: nazivPodatak, nazivTabele: nazivTabele, izvorni: izvorni, nazivPoslovniDomen: nazivPoslovniDomen,
                        idSemantickiDomen: domenSemantickiID, idDomen: idDomen, nazivAtributDomena: "Садржај", napomena: napomena, idPrimitivanTip: idPrimTip, ogranicenjaPDomena: ogranicenjaPoslovniDomenJson};
                    podaciListaJson.push(podatakJson);
                    var stringPodaci = JSON.stringify(podaciListaJson);
                    document.getElementById("podaciZaInsertJson").value = stringPodaci;
                    ogranicenjaPoslovniDomenJson = [];
                    var tblTbodyPostojecaO = document.getElementById("tbodyOgranicenja");
                    tblTbodyPostojecaO.innerHTML = "";

                    var tblTbodyPostojeca = document.getElementById("tbodyPodaci");
                    //TABLE ROWS
                    var tr = document.createElement('TR');
                    var td1 = document.createElement('TD');
                    td1.appendChild(document.createTextNode(nazivPodatak));
                    tr.appendChild(td1);
                    var td3 = document.createElement('TD');
                    if (document.getElementById("checkIzvorni").checked == true) {
                        td3.appendChild(document.createTextNode(nazivPoslovniDomen));
                    } else {
                        td3.appendChild(document.createTextNode(domen));
                    }
                    tr.appendChild(td3);
                    var td4 = document.createElement('TD');
                    var a = document.createElement("a");
                    a.setAttribute("class", "btn btn-primary, button-delete");
                    a.setAttribute("onclick", "obrisiPodatakIzmena(this);");
                    a.setAttribute("style", "width:70px; height:25px;padding:0px 0px 0px 0px!important;float:right");
                    a.text = "Обриши";
                    td4.appendChild(a);
                    tr.appendChild(td4);
                    tblTbodyPostojeca.appendChild(tr);
                    obrisiUnos();
                } else
                    alert("Податак " + nazivPodatak + " је већ додат!");
            }

            function obrisiUnos() {
                document.getElementById("nazivPodatka").value = "";
                document.getElementById("napomena").value = "";
                document.getElementById("domenSemanticki").selectedIndex = 0;
                document.getElementById("domen").selectedIndex = 0;
                document.getElementById("nazivPoslovnogDomena").value = "";
                document.getElementById("vrstaOsobine").innerHTML = "";
            }

            function dodajOgranicenje() {
                var vrstaOcombo = document.getElementById("vrstaOsobine");
                //var vrstaO = vrstaOcombo.options[vrstaOcombo.selectedIndex].value;
                //var vrstaONaziv = vrstaOcombo.options[vrstaOcombo.selectedIndex].text;
                var vrednostO = document.getElementById("ogranicenjeVrednosti").value;
                if (vrstaOcombo.innerText == "") {
                    alert("Морате изабрати врсту ограничења!");
                } else if (vrednostO != "") {
                    var provera = false;
                    for (var i in ogranicenjaPoslovniDomenJson) {
                        if (ogranicenjaPoslovniDomenJson[i].idVrstaOsobine == vrstaOcombo.options[vrstaOcombo.selectedIndex].value) {
                            provera = true;
                            break;
                        }
                    }
                    if (provera) {
                        alert("Врста ограничења је већ унета!");
                    } else {
                        var ogranicenjeJson = {idVrstaOsobine: vrstaOcombo.options[vrstaOcombo.selectedIndex].value, vrednostOgranicenja: vrednostO};
                        ogranicenjaPoslovniDomenJson.push(ogranicenjeJson);
                        var tblTbodyPostojeca = document.getElementById("tbodyOgranicenja");
                        var tr = document.createElement('TR');
                        var td1 = document.createElement('TD');
                        td1.appendChild(document.createTextNode(vrstaOcombo.options[vrstaOcombo.selectedIndex].text));
                        tr.appendChild(td1);
                        var td2 = document.createElement('TD');
                        td2.appendChild(document.createTextNode(vrednostO));
                        tr.appendChild(td2);
                        tblTbodyPostojeca.appendChild(tr);
                        document.getElementById("ogranicenjeVrednosti").value = "";
                    }
                } else {
                    alert("Морате унети вредност ограничења!");
                }

            }


            var poruka = <%=poruka%>;
            if (poruka == 1) {
                alert("Систем је запамтио измене регистра!");
            } else if (poruka == 2) {
                alert("Неисправан датум! Систем не може да запамти измене регистра!");
            } else if (poruka == 3) {
                alert("Систем не може да запамти измене регистра!");
            }
        </script>
        <script src="/resursi/js/popuni-combo-vrsta-osobine.js"></script>
        <script src="/resursi/js/readonly-datum.js"></script>
        <script src="/resursi/js/check-izvorni-podatak.js"></script>
        <script src="/resursi/js/obrisi-podatak-registra.js"></script>
        <script src="/resursi/vendor/jquery/jquery.min.js"></script>
        <script src="/resursi/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/resursi/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="/resursi/vendor/datatables/jquery.dataTables.js"></script>
        <script src="/resursi/vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/resursi/js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="/resursi/js/demo/datatables-demo.js"></script>
    </body>
</html>