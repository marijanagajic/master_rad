<%@page language="java" import="java.util.*" contentType="text/html" pageEncoding="UTF-8"%>     
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Спремни за одлазак?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Притисни "Одјави се" ако желиш да напустиш тренутну сесију.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Одустани</button>
                        <a class="btn btn-primary" href="/logout">Одјави се</a>
                    </div>
                </div>
            </div>
        </div>
