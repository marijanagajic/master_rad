<%@page import="com.ite.metaregistar.model.Registar"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Метарегистар</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="/resursi/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="/resursi/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="/resursi/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <link href="/resursi/home.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="/resursi/css/sb-admin.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <div>  
            <%@include file="top.jsp" %>
            <div id="wrapper">
                <div id="content-wrapper">
                    <div class="container-fluid">
                        <!-- Breadcrumbs-->
                        <ol class="breadcrumb"> <li class="breadcrumb-item active">Добро дошли!</li> 
                        </ol>
                    </div>
                    <%                        List<Registar> registriAll = (List<Registar>) request.getAttribute("listaRegistri");
                        Integer poruka = (Integer) request.getAttribute("poruka");
                    %>

                    <div class="card mb-3" style="width: 90%; margin-left: 5%">
                        <div class="card-header">
                            Регистри</div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <th>Назив регистра</th>
                                    <th>Орган</th>
                                    <th>Надлежност</th>
                                    <th>Рок чувања података</th>
                                    <th>Електронски облик</th>
                                    </thead>
                                    <tbody>
                                        <% for (Registar reg : registriAll) {%>
                                        <tr>
                                            <td><a href ="/user/registar/pregled/<%=reg.getIdRegistar()%>"><%=reg.getNazivRegistar()%></a></td>
                                            <td><%=reg.getIdOrgan().getNazivOrgana()%></td>
                                            <td><%=reg.getIdOsnovNadleznosti().getNazivNadleznost()%></td>
                                            <td><%=reg.getIdRokCuvanjaPodataka().getNazivRokCuvanjaPodataka()%></td>
                                            <td><%=((reg.getUElektronskomObliku()) ? "Да" : "Не")%></td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <%@include  file="bottom.jsp"%>
                </div>
                <!-- /.content-wrapper -->
            </div>
        </div>
        <%@include file="logout.jsp" %>
        <script type="text/javascript">
            var poruka = <%=poruka%>;
            if (poruka == 1) {
                alert("Систем је запамтио регистар!");
            } else if (poruka == 2) {
                alert("Неисправан датум! Систем не може да запамти регистар!");
            } else if (poruka == 3) {
                alert("Систем не може да запамти регистар!");
            }
        </script>
        <script src="/resursi/vendor/jquery/jquery.min.js"></script>
        <script src="/resursi/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/resursi/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="/resursi/vendor/chart.js/Chart.min.js"></script>
        <script src="/resursi/vendor/datatables/jquery.dataTables.js"></script>
        <script src="/resursi/vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/resursi/js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="/resursi/js/demo/datatables-demo.js"></script>
        <script src="/resursi/js/demo/chart-area-demo.js"></script>
    </body>
</html>