/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author SVETLANA.LOKAL
 */
@Entity
@Table(name = "primitivan_tip")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrimitivanTip.findAll", query = "SELECT p FROM PrimitivanTip p")
    , @NamedQuery(name = "PrimitivanTip.findByIdPrimitivanTip", query = "SELECT p FROM PrimitivanTip p WHERE p.idPrimitivanTip = :idPrimitivanTip")
    , @NamedQuery(name = "PrimitivanTip.findByNazivPrimitivanTip", query = "SELECT p FROM PrimitivanTip p WHERE p.nazivPrimitivanTip = :nazivPrimitivanTip")})
public class PrimitivanTip implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "idPrimitivanTip")
    private Integer idPrimitivanTip;
    @Size(max = 250)
    @Column(name = "nazivPrimitivanTip")
    private String nazivPrimitivanTip;
    @JoinTable(name = "osobine_primitivnog_tipa", joinColumns = {
        @JoinColumn(name = "idPrimitivanTip", referencedColumnName = "idPrimitivanTip")}, inverseJoinColumns = {
        @JoinColumn(name = "idVrstaOsobine", referencedColumnName = "idVrstaOsobine")})
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<VrstaOsobine> vrstaOsobineCollection;
    @OneToMany(mappedBy = "idPrimitivanTip")
    private Collection<AtributDomena> atributDomenaCollection;

    public PrimitivanTip() {
    }

    public PrimitivanTip(Integer idPrimitivanTip) {
        this.idPrimitivanTip = idPrimitivanTip;
    }

    public Integer getIdPrimitivanTip() {
        return idPrimitivanTip;
    }

    public void setIdPrimitivanTip(Integer idPrimitivanTip) {
        this.idPrimitivanTip = idPrimitivanTip;
    }

    public String getNazivPrimitivanTip() {
        return nazivPrimitivanTip;
    }

    public void setNazivPrimitivanTip(String nazivPrimitivanTip) {
        this.nazivPrimitivanTip = nazivPrimitivanTip;
    }

    @XmlTransient
    public Collection<VrstaOsobine> getVrstaOsobineCollection() {
        return vrstaOsobineCollection;
    }

    public void setVrstaOsobineCollection(Collection<VrstaOsobine> vrstaOsobineCollection) {
        this.vrstaOsobineCollection = vrstaOsobineCollection;
    }

    @XmlTransient
    public Collection<AtributDomena> getAtributDomenaCollection() {
        return atributDomenaCollection;
    }

    public void setAtributDomenaCollection(Collection<AtributDomena> atributDomenaCollection) {
        this.atributDomenaCollection = atributDomenaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrimitivanTip != null ? idPrimitivanTip.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrimitivanTip)) {
            return false;
        }
        PrimitivanTip other = (PrimitivanTip) object;
        if ((this.idPrimitivanTip == null && other.idPrimitivanTip != null) || (this.idPrimitivanTip != null && !this.idPrimitivanTip.equals(other.idPrimitivanTip))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.PrimitivanTip[ idPrimitivanTip=" + idPrimitivanTip + " ]";
    }

}
