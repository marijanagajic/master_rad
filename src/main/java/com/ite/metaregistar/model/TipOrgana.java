/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "tip_organa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipOrgana.findAll", query = "SELECT t FROM TipOrgana t")
    , @NamedQuery(name = "TipOrgana.findByIdTipOrgana", query = "SELECT t FROM TipOrgana t WHERE t.idTipOrgana = :idTipOrgana")
    , @NamedQuery(name = "TipOrgana.findByNazivTipOrgana", query = "SELECT t FROM TipOrgana t WHERE t.nazivTipOrgana = :nazivTipOrgana")})
public class TipOrgana implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idTipOrgana")
    private Integer idTipOrgana;
    @Column(name = "nazivTipOrgana")
    private String nazivTipOrgana;
    @OneToMany(mappedBy = "idTipOrgana")
    private Collection<Organ> organCollection;

    public TipOrgana() {
    }

    public TipOrgana(Integer idTipOrgana) {
        this.idTipOrgana = idTipOrgana;
    }

    public Integer getIdTipOrgana() {
        return idTipOrgana;
    }

    public void setIdTipOrgana(Integer idTipOrgana) {
        this.idTipOrgana = idTipOrgana;
    }

    public String getNazivTipOrgana() {
        return nazivTipOrgana;
    }

    public void setNazivTipOrgana(String nazivTipOrgana) {
        this.nazivTipOrgana = nazivTipOrgana;
    }

    @XmlTransient
    public Collection<Organ> getOrganCollection() {
        return organCollection;
    }

    public void setOrganCollection(Collection<Organ> organCollection) {
        this.organCollection = organCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipOrgana != null ? idTipOrgana.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipOrgana)) {
            return false;
        }
        TipOrgana other = (TipOrgana) object;
        if ((this.idTipOrgana == null && other.idTipOrgana != null) || (this.idTipOrgana != null && !this.idTipOrgana.equals(other.idTipOrgana))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.TipOrgana[ idTipOrgana=" + idTipOrgana + " ]";
    }
    
}
