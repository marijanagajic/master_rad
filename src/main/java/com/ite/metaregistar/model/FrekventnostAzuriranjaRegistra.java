/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "frekventnost_azuriranja_registra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FrekventnostAzuriranjaRegistra.findAll", query = "SELECT f FROM FrekventnostAzuriranjaRegistra f")
    , @NamedQuery(name = "FrekventnostAzuriranjaRegistra.findByIdFrekventnostAzuriranjaRegistra", query = "SELECT f FROM FrekventnostAzuriranjaRegistra f WHERE f.idFrekventnostAzuriranjaRegistra = :idFrekventnostAzuriranjaRegistra")
    , @NamedQuery(name = "FrekventnostAzuriranjaRegistra.findByNazivFrekventnostAzuriranjaRegistra", query = "SELECT f FROM FrekventnostAzuriranjaRegistra f WHERE f.nazivFrekventnostAzuriranjaRegistra = :nazivFrekventnostAzuriranjaRegistra")})
public class FrekventnostAzuriranjaRegistra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idFrekventnostAzuriranjaRegistra")
    private Integer idFrekventnostAzuriranjaRegistra;
    @Column(name = "nazivFrekventnostAzuriranjaRegistra")
    private String nazivFrekventnostAzuriranjaRegistra;
    @OneToMany(mappedBy = "idFrekventnostAzuriranjaRegistra")
    private Collection<Registar> registarCollection;

    public FrekventnostAzuriranjaRegistra() {
    }

    public FrekventnostAzuriranjaRegistra(Integer idFrekventnostAzuriranjaRegistra) {
        this.idFrekventnostAzuriranjaRegistra = idFrekventnostAzuriranjaRegistra;
    }

    public Integer getIdFrekventnostAzuriranjaRegistra() {
        return idFrekventnostAzuriranjaRegistra;
    }

    public void setIdFrekventnostAzuriranjaRegistra(Integer idFrekventnostAzuriranjaRegistra) {
        this.idFrekventnostAzuriranjaRegistra = idFrekventnostAzuriranjaRegistra;
    }

    public String getNazivFrekventnostAzuriranjaRegistra() {
        return nazivFrekventnostAzuriranjaRegistra;
    }

    public void setNazivFrekventnostAzuriranjaRegistra(String nazivFrekventnostAzuriranjaRegistra) {
        this.nazivFrekventnostAzuriranjaRegistra = nazivFrekventnostAzuriranjaRegistra;
    }

    @XmlTransient
    public Collection<Registar> getRegistarCollection() {
        return registarCollection;
    }

    public void setRegistarCollection(Collection<Registar> registarCollection) {
        this.registarCollection = registarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFrekventnostAzuriranjaRegistra != null ? idFrekventnostAzuriranjaRegistra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FrekventnostAzuriranjaRegistra)) {
            return false;
        }
        FrekventnostAzuriranjaRegistra other = (FrekventnostAzuriranjaRegistra) object;
        if ((this.idFrekventnostAzuriranjaRegistra == null && other.idFrekventnostAzuriranjaRegistra != null) || (this.idFrekventnostAzuriranjaRegistra != null && !this.idFrekventnostAzuriranjaRegistra.equals(other.idFrekventnostAzuriranjaRegistra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra[ idFrekventnostAzuriranjaRegistra=" + idFrekventnostAzuriranjaRegistra + " ]";
    }
    
}
