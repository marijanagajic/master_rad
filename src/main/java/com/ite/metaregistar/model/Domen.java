/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "domen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Domen.findAll", query = "SELECT d FROM Domen d")
    , @NamedQuery(name = "Domen.findByIdDomen", query = "SELECT d FROM Domen d WHERE d.idDomen = :idDomen")
    , @NamedQuery(name = "Domen.findByNazivDomen", query = "SELECT d FROM Domen d WHERE d.nazivDomen = :nazivDomen")})
public class Domen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDomen")
    private Integer idDomen;
    @Column(name = "nazivDomen")
    private String nazivDomen;
    @OneToMany(mappedBy = "idDomen")
    private Collection<EntitetRegistra> entitetRegistraCollection;
    @OneToMany(mappedBy = "idDomenBaziranNa")
    private Collection<Domen> domenCollection;
    @JoinColumn(name = "idDomenBaziranNa", referencedColumnName = "idDomen")
    @ManyToOne
    private Domen idDomenBaziranNa;
    @JoinColumn(name = "idTipDomena", referencedColumnName = "idTipDomena")
    @ManyToOne
    private TipDomena idTipDomena;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idDomen")
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<AtributDomena> atributDomenaCollection;

    public Domen() {
    }

    public Domen(Integer idDomen) {
        this.idDomen = idDomen;
    }

    public Integer getIdDomen() {
        return idDomen;
    }

    public void setIdDomen(Integer idDomen) {
        this.idDomen = idDomen;
    }

    public String getNazivDomen() {
        return nazivDomen;
    }

    public void setNazivDomen(String nazivDomen) {
        this.nazivDomen = nazivDomen;
    }

    @XmlTransient
    public Collection<EntitetRegistra> getEntitetRegistraCollection() {
        return entitetRegistraCollection;
    }

    public void setEntitetRegistraCollection(Collection<EntitetRegistra> entitetRegistraCollection) {
        this.entitetRegistraCollection = entitetRegistraCollection;
    }

    @XmlTransient
    public Collection<Domen> getDomenCollection() {
        return domenCollection;
    }

    public void setDomenCollection(Collection<Domen> domenCollection) {
        this.domenCollection = domenCollection;
    }

    public Domen getIdDomenBaziranNa() {
        return idDomenBaziranNa;
    }

    public void setIdDomenBaziranNa(Domen idDomenBaziranNa) {
        this.idDomenBaziranNa = idDomenBaziranNa;
    }

    public TipDomena getIdTipDomena() {
        return idTipDomena;
    }

    public void setIdTipDomena(TipDomena idTipDomena) {
        this.idTipDomena = idTipDomena;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDomen != null ? idDomen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Domen)) {
            return false;
        }
        Domen other = (Domen) object;
        if ((this.idDomen == null && other.idDomen != null) || (this.idDomen != null && !this.idDomen.equals(other.idDomen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.Domen[ idDomen=" + idDomen + " ]";
    }

    @XmlTransient
    public Collection<AtributDomena> getAtributDomenaCollection() {
        return atributDomenaCollection;
    }

    public void setAtributDomenaCollection(Collection<AtributDomena> atributDomenaCollection) {
        this.atributDomenaCollection = atributDomenaCollection;
    }

}
