/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SVETLANA.LOKAL
 */
@Entity
@Table(name = "vrsta_osobine")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VrstaOsobine.findAll", query = "SELECT v FROM VrstaOsobine v")
    , @NamedQuery(name = "VrstaOsobine.findByIdVrstaOsobine", query = "SELECT v FROM VrstaOsobine v WHERE v.idVrstaOsobine = :idVrstaOsobine")
    , @NamedQuery(name = "VrstaOsobine.findByNazivVrstaOsobine", query = "SELECT v FROM VrstaOsobine v WHERE v.nazivVrstaOsobine = :nazivVrstaOsobine")})
public class VrstaOsobine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "idVrstaOsobine")
    private Integer idVrstaOsobine;
    @Size(max = 250)
    @Column(name = "nazivVrstaOsobine")
    private String nazivVrstaOsobine;
    @ManyToMany(mappedBy = "vrstaOsobineCollection")
    private Collection<PrimitivanTip> primitivanTipCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vrstaOsobine")
    private Collection<OgranicenjeVrednosti> ogranicenjeVrednostiCollection;

    public VrstaOsobine() {
    }

    public VrstaOsobine(Integer idVrstaOsobine) {
        this.idVrstaOsobine = idVrstaOsobine;
    }

    public Integer getIdVrstaOsobine() {
        return idVrstaOsobine;
    }

    public void setIdVrstaOsobine(Integer idVrstaOsobine) {
        this.idVrstaOsobine = idVrstaOsobine;
    }

    public String getNazivVrstaOsobine() {
        return nazivVrstaOsobine;
    }

    public void setNazivVrstaOsobine(String nazivVrstaOsobine) {
        this.nazivVrstaOsobine = nazivVrstaOsobine;
    }

    @XmlTransient
    public Collection<PrimitivanTip> getPrimitivanTipCollection() {
        return primitivanTipCollection;
    }

    public void setPrimitivanTipCollection(Collection<PrimitivanTip> primitivanTipCollection) {
        this.primitivanTipCollection = primitivanTipCollection;
    }

    @XmlTransient
    public Collection<OgranicenjeVrednosti> getOgranicenjeVrednostiCollection() {
        return ogranicenjeVrednostiCollection;
    }

    public void setOgranicenjeVrednostiCollection(Collection<OgranicenjeVrednosti> ogranicenjeVrednostiCollection) {
        this.ogranicenjeVrednostiCollection = ogranicenjeVrednostiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVrstaOsobine != null ? idVrstaOsobine.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VrstaOsobine)) {
            return false;
        }
        VrstaOsobine other = (VrstaOsobine) object;
        if ((this.idVrstaOsobine == null && other.idVrstaOsobine != null) || (this.idVrstaOsobine != null && !this.idVrstaOsobine.equals(other.idVrstaOsobine))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.VrstaOsobine[ idVrstaOsobine=" + idVrstaOsobine + " ]";
    }
    
}
