/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "entitet_registra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntitetRegistra.findAll", query = "SELECT e FROM EntitetRegistra e")
    , @NamedQuery(name = "EntitetRegistra.findByIdEntitetRegistra", query = "SELECT e FROM EntitetRegistra e WHERE e.idEntitetRegistra = :idEntitetRegistra")
    , @NamedQuery(name = "EntitetRegistra.findByNazivEntitetRegistra", query = "SELECT e FROM EntitetRegistra e WHERE e.nazivEntitetRegistra = :nazivEntitetRegistra")
    , @NamedQuery(name = "EntitetRegistra.findByIzvorniRegistar", query = "SELECT e FROM EntitetRegistra e WHERE e.izvorniRegistar = :izvorniRegistar")
    , @NamedQuery(name = "EntitetRegistra.findByNapomena", query = "SELECT e FROM EntitetRegistra e WHERE e.napomena = :napomena")})
public class EntitetRegistra extends Klijent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEntitetRegistra")
    private Integer idEntitetRegistra;
    @Column(name = "nazivEntitetRegistra")
    private String nazivEntitetRegistra;
    @Column(name = "nazivTabele")
    private String nazivTabele;
    @Column(name = "izvorniRegistar")
    private Boolean izvorniRegistar;
    @Column(name = "napomena")
    private String napomena;
    @JoinColumn(name = "idDomen", referencedColumnName = "idDomen")
    @ManyToOne
    private Domen idDomen;
    @JoinColumn(name = "idRegistar", referencedColumnName = "idRegistar")
    @ManyToOne
    private Registar idRegistar;
    
    public EntitetRegistra() {
    }

    public EntitetRegistra(Integer idEntitetRegistra) {
        this.idEntitetRegistra = idEntitetRegistra;
    }

    public EntitetRegistra(String nazivEntitetRegistra, String poslovniTerminEntitetRegistra, Boolean izvorniRegistar, Boolean nijeIzvorniRegistar, Boolean nepozantnaIzvornost, String napomena, Domen idDomen, Registar idRegistar, EntitetRegistra idEntitetIzvorni) {
        this.nazivEntitetRegistra = nazivEntitetRegistra;
        this.izvorniRegistar = izvorniRegistar;
        this.napomena = napomena;
        this.idDomen = idDomen;
        this.idRegistar = idRegistar;
    }

    public Integer getIdEntitetRegistra() {
        return idEntitetRegistra;
    }

    public void setIdEntitetRegistra(Integer idEntitetRegistra) {
        this.idEntitetRegistra = idEntitetRegistra;
    }

    public String getNazivEntitetRegistra() {
        return nazivEntitetRegistra;
    }

    public void setNazivEntitetRegistra(String nazivEntitetRegistra) {
        this.nazivEntitetRegistra = nazivEntitetRegistra;
    }

    public String getNazivTabele() {
        return nazivTabele;
    }

    public void setNazivTabele(String nazivTabele) {
        this.nazivTabele = nazivTabele;
    }

    public Boolean getIzvorniRegistar() {
        return izvorniRegistar;
    }

    public void setIzvorniRegistar(Boolean izvorniRegistar) {
        this.izvorniRegistar = izvorniRegistar;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Domen getIdDomen() {
        return idDomen;
    }

    public void setIdDomen(Domen idDomen) {
        this.idDomen = idDomen;
    }

    public Registar getIdRegistar() {
        return idRegistar;
    }

    public void setIdRegistar(Registar idRegistar) {
        this.idRegistar = idRegistar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntitetRegistra != null ? idEntitetRegistra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntitetRegistra)) {
            return false;
        }
        EntitetRegistra other = (EntitetRegistra) object;
        if ((this.idEntitetRegistra == null && other.idEntitetRegistra != null) || (this.idEntitetRegistra != null && !this.idEntitetRegistra.equals(other.idEntitetRegistra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.EntitetRegistra[ idEntitetRegistra=" + idEntitetRegistra + " ]";
    }

    @Override
    protected void posaljiInfoOPromeniVrednosti() {
       
        // Poziv servisa za obavestavanje konkretnog registra o promeni podatka
        
        //AKO SE OTKOMENTARISE METODA POZIVOM SERVISA http://localhost:8084/entitet/notify  BICE GRESKA JER SU LINKOVI 
        //PRAZNI U BAZI POSTO NISAM NAPRAVILA JOS SERVISE ZA SIMULACIJU IZMENA U BAZAMA KOJE PRIME OBAVESTENJE
        
        String uri = this.idRegistar.getUrl();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        IzmenjeniEntitet izm = this.novaVrednost;
        HttpEntity<IzmenjeniEntitet> request = new HttpEntity<IzmenjeniEntitet>(izm, headers);
        
        RestTemplate restTemplate = new RestTemplate();
        IzmenjeniEntitet response = restTemplate.postForObject(uri, request, IzmenjeniEntitet.class);
    }

}
