/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "tip_domena")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipDomena.findAll", query = "SELECT t FROM TipDomena t")
    , @NamedQuery(name = "TipDomena.findByIdTipDomena", query = "SELECT t FROM TipDomena t WHERE t.idTipDomena = :idTipDomena")
    , @NamedQuery(name = "TipDomena.findByNazivTipDomena", query = "SELECT t FROM TipDomena t WHERE t.nazivTipDomena = :nazivTipDomena")})
public class TipDomena implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTipDomena")
    private Integer idTipDomena;
    @Size(max = 250)
    @Column(name = "nazivTipDomena")
    private String nazivTipDomena;
    @OneToMany(mappedBy = "idTipDomena")
    private Collection<Domen> domenCollection;

    public TipDomena() {
    }

    public TipDomena(Integer idTipDomena) {
        this.idTipDomena = idTipDomena;
    }

    public Integer getIdTipDomena() {
        return idTipDomena;
    }

    public void setIdTipDomena(Integer idTipDomena) {
        this.idTipDomena = idTipDomena;
    }

    public String getNazivTipDomena() {
        return nazivTipDomena;
    }

    public void setNazivTipDomena(String nazivTipDomena) {
        this.nazivTipDomena = nazivTipDomena;
    }

    @XmlTransient
    public Collection<Domen> getDomenCollection() {
        return domenCollection;
    }

    public void setDomenCollection(Collection<Domen> domenCollection) {
        this.domenCollection = domenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipDomena != null ? idTipDomena.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipDomena)) {
            return false;
        }
        TipDomena other = (TipDomena) object;
        if ((this.idTipDomena == null && other.idTipDomena != null) || (this.idTipDomena != null && !this.idTipDomena.equals(other.idTipDomena))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.meta.model.TipDomena[ idTipDomena=" + idTipDomena + " ]";
    }
    
}
