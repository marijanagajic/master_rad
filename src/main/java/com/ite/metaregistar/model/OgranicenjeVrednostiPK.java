/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author SVETLANA.LOKAL
 */
@Embeddable
public class OgranicenjeVrednostiPK implements Serializable {

    @NotNull
    @Column(name = "idVrstaOsobine")
    private int idVrstaOsobine;
    @NotNull
    @Column(name = "idAtributDomena")
    private int idAtributDomena;

    public OgranicenjeVrednostiPK() {
    }

    public OgranicenjeVrednostiPK(int idVrstaOsobine, int idAtributDomena) {
        this.idVrstaOsobine = idVrstaOsobine;
        this.idAtributDomena = idAtributDomena;
    }

    public int getIdVrstaOsobine() {
        return idVrstaOsobine;
    }

    public void setIdVrstaOsobine(int idVrstaOsobine) {
        this.idVrstaOsobine = idVrstaOsobine;
    }

    public int getIdAtributDomena() {
        return idAtributDomena;
    }

    public void setIdAtributDomena(int idAtributDomena) {
        this.idAtributDomena = idAtributDomena;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idVrstaOsobine;
        hash += (int) idAtributDomena;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OgranicenjeVrednostiPK)) {
            return false;
        }
        OgranicenjeVrednostiPK other = (OgranicenjeVrednostiPK) object;
        if (this.idVrstaOsobine != other.idVrstaOsobine) {
            return false;
        }
        if (this.idAtributDomena != other.idAtributDomena) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.OgranicenjeVrednostiPK[ idVrstaOsobine=" + idVrstaOsobine + ", idAtributDomena=" + idAtributDomena + " ]";
    }
    
}
