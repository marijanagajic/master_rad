/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "rok_cuvanja_podataka")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RokCuvanjaPodataka.findAll", query = "SELECT r FROM RokCuvanjaPodataka r")
    , @NamedQuery(name = "RokCuvanjaPodataka.findByIdRokCuvanjaPodataka", query = "SELECT r FROM RokCuvanjaPodataka r WHERE r.idRokCuvanjaPodataka = :idRokCuvanjaPodataka")
    , @NamedQuery(name = "RokCuvanjaPodataka.findByNazivRokCuvanjaPodataka", query = "SELECT r FROM RokCuvanjaPodataka r WHERE r.nazivRokCuvanjaPodataka = :nazivRokCuvanjaPodataka")})
public class RokCuvanjaPodataka implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idRokCuvanjaPodataka")
    private Integer idRokCuvanjaPodataka;
    @Column(name = "nazivRokCuvanjaPodataka")
    private String nazivRokCuvanjaPodataka;
    @OneToMany(mappedBy = "idRokCuvanjaPodataka")
    private Collection<Registar> registarCollection;

    public RokCuvanjaPodataka() {
    }

    public RokCuvanjaPodataka(Integer idRokCuvanjaPodataka) {
        this.idRokCuvanjaPodataka = idRokCuvanjaPodataka;
    }

    public Integer getIdRokCuvanjaPodataka() {
        return idRokCuvanjaPodataka;
    }

    public void setIdRokCuvanjaPodataka(Integer idRokCuvanjaPodataka) {
        this.idRokCuvanjaPodataka = idRokCuvanjaPodataka;
    }

    public String getNazivRokCuvanjaPodataka() {
        return nazivRokCuvanjaPodataka;
    }

    public void setNazivRokCuvanjaPodataka(String nazivRokCuvanjaPodataka) {
        this.nazivRokCuvanjaPodataka = nazivRokCuvanjaPodataka;
    }

    @XmlTransient
    public Collection<Registar> getRegistarCollection() {
        return registarCollection;
    }

    public void setRegistarCollection(Collection<Registar> registarCollection) {
        this.registarCollection = registarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRokCuvanjaPodataka != null ? idRokCuvanjaPodataka.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RokCuvanjaPodataka)) {
            return false;
        }
        RokCuvanjaPodataka other = (RokCuvanjaPodataka) object;
        if ((this.idRokCuvanjaPodataka == null && other.idRokCuvanjaPodataka != null) || (this.idRokCuvanjaPodataka != null && !this.idRokCuvanjaPodataka.equals(other.idRokCuvanjaPodataka))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.RokCuvanjaPodataka[ idRokCuvanjaPodataka=" + idRokCuvanjaPodataka + " ]";
    }
    
}
