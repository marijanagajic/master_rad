/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "pravni_osnov")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PravniOsnov.findAll", query = "SELECT p FROM PravniOsnov p")
    , @NamedQuery(name = "PravniOsnov.findByIdPravniOsnov", query = "SELECT p FROM PravniOsnov p WHERE p.idPravniOsnov = :idPravniOsnov")
    , @NamedQuery(name = "PravniOsnov.findByNazivPravniOsnov", query = "SELECT p FROM PravniOsnov p WHERE p.nazivPravniOsnov = :nazivPravniOsnov")})
public class PravniOsnov implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idPravniOsnov")
    private Integer idPravniOsnov;
    @Column(name = "nazivPravniOsnov")
    private String nazivPravniOsnov;
    @OneToMany(mappedBy = "idPravniOsnov")
    private Collection<Registar> registarCollection;

    public PravniOsnov() {
    }

    public PravniOsnov(Integer idPravniOsnov) {
        this.idPravniOsnov = idPravniOsnov;
    }

    public Integer getIdPravniOsnov() {
        return idPravniOsnov;
    }

    public void setIdPravniOsnov(Integer idPravniOsnov) {
        this.idPravniOsnov = idPravniOsnov;
    }

    public String getNazivPravniOsnov() {
        return nazivPravniOsnov;
    }

    public void setNazivPravniOsnov(String nazivPravniOsnov) {
        this.nazivPravniOsnov = nazivPravniOsnov;
    }

    @XmlTransient
    public Collection<Registar> getRegistarCollection() {
        return registarCollection;
    }

    public void setRegistarCollection(Collection<Registar> registarCollection) {
        this.registarCollection = registarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPravniOsnov != null ? idPravniOsnov.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PravniOsnov)) {
            return false;
        }
        PravniOsnov other = (PravniOsnov) object;
        if ((this.idPravniOsnov == null && other.idPravniOsnov != null) || (this.idPravniOsnov != null && !this.idPravniOsnov.equals(other.idPravniOsnov))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.PravniOsnov[ idPravniOsnov=" + idPravniOsnov + " ]";
    }
    
}
