/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import com.ite.metaregistar.patern.Observable;

/**
 *
 * @author Marijana
 */
public abstract class Klijent {

    protected IzmenjeniEntitet novaVrednost;
    private Observable observable;

//    public Klijent(Observable observable) {
//        this.observable = observable;
//        this.novaVrednost = observable.getNovaVrednost();
//    }

    public void setObservable(Observable observable) {
        this.observable = observable;
    }

    public void osvezi() {
        this.novaVrednost = observable.getNovaVrednost();
        posaljiInfoOPromeniVrednosti();
    }

    abstract protected void posaljiInfoOPromeniVrednosti();

}
