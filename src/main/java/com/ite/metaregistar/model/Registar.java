/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "registar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registar.findAll", query = "SELECT r FROM Registar r")
    , @NamedQuery(name = "Registar.findByIdRegistar", query = "SELECT r FROM Registar r WHERE r.idRegistar = :idRegistar")
    , @NamedQuery(name = "Registar.findByNazivRegistar", query = "SELECT r FROM Registar r WHERE r.nazivRegistar = :nazivRegistar")
    , @NamedQuery(name = "Registar.findByDatumUspostavljanja", query = "SELECT r FROM Registar r WHERE r.datumUspostavljanja = :datumUspostavljanja")
    , @NamedQuery(name = "Registar.findByUElektronskomObliku", query = "SELECT r FROM Registar r WHERE r.uElektronskomObliku = :uElektronskomObliku")
    , @NamedQuery(name = "Registar.findByNapomena", query = "SELECT r FROM Registar r WHERE r.napomena = :napomena")})
public class Registar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idRegistar")
    private Integer idRegistar;
    @Column(name = "nazivRegistar")
    private String nazivRegistar;
    @Column(name = "datumUspostavljanja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumUspostavljanja;
    @Column(name = "uElektronskomObliku")
    private Boolean uElektronskomObliku;
    @Column(name = "napomena")
    private String napomena;
    @Column(name = "datumUnosa")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumUnosa;
    @Column(name = "propisNevazeci")
    private String propisNevazeci;
    @Column(name = "url")
    private String url;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idRegistar")
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<EntitetRegistra> entitetRegistraCollection;
    @JoinColumn(name = "idFrekventnostAzuriranjaRegistra", referencedColumnName = "idFrekventnostAzuriranjaRegistra")
    @ManyToOne
    private FrekventnostAzuriranjaRegistra idFrekventnostAzuriranjaRegistra;
    @JoinColumn(name = "idKorisnik", referencedColumnName = "idKorisnik")
    @ManyToOne
    private Korisnik idKorisnik;
    @JoinColumn(name = "idOrgan", referencedColumnName = "idOrgan")
    @ManyToOne
    private Organ idOrgan;
    @JoinColumn(name = "idOsnovNadleznosti", referencedColumnName = "idNadleznost")
    @ManyToOne
    private OsnovNadleznosti idOsnovNadleznosti;
    @JoinColumn(name = "idPravniOsnov", referencedColumnName = "idPravniOsnov")
    @ManyToOne
    private PravniOsnov idPravniOsnov;
    @JoinColumn(name = "idRokCuvanjaPodataka", referencedColumnName = "idRokCuvanjaPodataka")
    @ManyToOne
    private RokCuvanjaPodataka idRokCuvanjaPodataka;

    public Registar() {
    }

    public Registar(Integer idRegistar) {
        this.idRegistar = idRegistar;
    }

    public Registar(Integer idRegistar, String nazivRegistar) {
        this.idRegistar = idRegistar;
        this.nazivRegistar = nazivRegistar;
    }

    public Integer getIdRegistar() {
        return idRegistar;
    }

    public void setIdRegistar(Integer idRegistar) {
        this.idRegistar = idRegistar;
    }

    public String getNazivRegistar() {
        return nazivRegistar;
    }

    public void setNazivRegistar(String nazivRegistar) {
        this.nazivRegistar = nazivRegistar;
    }

    public Date getDatumUspostavljanja() {
        return datumUspostavljanja;
    }

    public void setDatumUspostavljanja(Date datumUspostavljanja) {
        this.datumUspostavljanja = datumUspostavljanja;
    }
    
    public Boolean getUElektronskomObliku() {
        return uElektronskomObliku;
    }

    public void setUElektronskomObliku(Boolean uElektronskomObliku) {
        this.uElektronskomObliku = uElektronskomObliku;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public String getPropisNevazeci() {
        return propisNevazeci;
    }

    public void setPropisNevazeci(String propisNevazeci) {
        this.propisNevazeci = propisNevazeci;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlTransient
    public Collection<EntitetRegistra> getEntitetRegistraCollection() {
        return entitetRegistraCollection;
    }

    public void setEntitetRegistraCollection(Collection<EntitetRegistra> entitetRegistraCollection) {
        this.entitetRegistraCollection = entitetRegistraCollection;
    }

    public FrekventnostAzuriranjaRegistra getIdFrekventnostAzuriranjaRegistra() {
        return idFrekventnostAzuriranjaRegistra;
    }

    public void setIdFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra idFrekventnostAzuriranjaRegistra) {
        this.idFrekventnostAzuriranjaRegistra = idFrekventnostAzuriranjaRegistra;
    }

    public Organ getIdOrgan() {
        return idOrgan;
    }

    public void setIdOrgan(Organ idOrgan) {
        this.idOrgan = idOrgan;
    }

    public OsnovNadleznosti getIdOsnovNadleznosti() {
        return idOsnovNadleznosti;
    }

    public void setIdOsnovNadleznosti(OsnovNadleznosti idOsnovNadleznosti) {
        this.idOsnovNadleznosti = idOsnovNadleznosti;
    }

    public PravniOsnov getIdPravniOsnov() {
        return idPravniOsnov;
    }

    public void setIdPravniOsnov(PravniOsnov idPravniOsnov) {
        this.idPravniOsnov = idPravniOsnov;
    }

    public RokCuvanjaPodataka getIdRokCuvanjaPodataka() {
        return idRokCuvanjaPodataka;
    }

    public void setIdRokCuvanjaPodataka(RokCuvanjaPodataka idRokCuvanjaPodataka) {
        this.idRokCuvanjaPodataka = idRokCuvanjaPodataka;
    }

    public Date getDatumUnosa() {
        return datumUnosa;
    }

    public void setDatumUnosa(Date datumUnosa) {
        this.datumUnosa = datumUnosa;
    }

    public Korisnik getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(Korisnik idKorisnik) {
        this.idKorisnik = idKorisnik;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistar != null ? idRegistar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registar)) {
            return false;
        }
        Registar other = (Registar) object;
        if ((this.idRegistar == null && other.idRegistar != null) || (this.idRegistar != null && !this.idRegistar.equals(other.idRegistar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.Registar[ idRegistar=" + idRegistar + " ]";
    }
    
}
