/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import com.ite.metaregistar.model.Domen;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author SVETLANA.LOKAL
 */
@Entity
@Table(name = "atribut_domena")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtributDomena.findAll", query = "SELECT a FROM AtributDomena a")
    , @NamedQuery(name = "AtributDomena.findByIdAtributDomena", query = "SELECT a FROM AtributDomena a WHERE a.idAtributDomena = :idAtributDomena")
    , @NamedQuery(name = "AtributDomena.findByNazivAtributDomena", query = "SELECT a FROM AtributDomena a WHERE a.nazivAtributDomena = :nazivAtributDomena")
    , @NamedQuery(name = "AtributDomena.findByObavezan", query = "SELECT a FROM AtributDomena a WHERE a.obavezan = :obavezan")})
public class AtributDomena implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "idAtributDomena")
    private Integer idAtributDomena;
    @Size(max = 250)
    @Column(name = "nazivAtributDomena")
    private String nazivAtributDomena;
    @Column(name = "obavezan")
    private Boolean obavezan;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "atributDomena")
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<OgranicenjeVrednosti> ogranicenjeVrednostiCollection;
    @JoinColumn(name = "idDomen", referencedColumnName = "idDomen")
    @ManyToOne
    private Domen idDomen;
    @JoinColumn(name = "idPrimitivanTip", referencedColumnName = "idPrimitivanTip")
    @ManyToOne
    private PrimitivanTip idPrimitivanTip;
    @JoinColumn(name = "idTipAtributDomena", referencedColumnName = "idTipAtributDomena")
    @ManyToOne
    private TipAtributDomena idTipAtributDomena;

    public AtributDomena() {
    }

    public AtributDomena(Integer idAtributDomena) {
        this.idAtributDomena = idAtributDomena;
    }

    public Integer getIdAtributDomena() {
        return idAtributDomena;
    }

    public void setIdAtributDomena(Integer idAtributDomena) {
        this.idAtributDomena = idAtributDomena;
    }

    public String getNazivAtributDomena() {
        return nazivAtributDomena;
    }

    public void setNazivAtributDomena(String nazivAtributDomena) {
        this.nazivAtributDomena = nazivAtributDomena;
    }

    public Boolean getObavezan() {
        return obavezan;
    }

    public void setObavezan(Boolean obavezan) {
        this.obavezan = obavezan;
    }

    @XmlTransient
    public Collection<OgranicenjeVrednosti> getOgranicenjeVrednostiCollection() {
        return ogranicenjeVrednostiCollection;
    }

    public void setOgranicenjeVrednostiCollection(Collection<OgranicenjeVrednosti> ogranicenjeVrednostiCollection) {
        this.ogranicenjeVrednostiCollection = ogranicenjeVrednostiCollection;
    }

    public Domen getIdDomen() {
        return idDomen;
    }

    public void setIdDomen(Domen idDomen) {
        this.idDomen = idDomen;
    }

    public PrimitivanTip getIdPrimitivanTip() {
        return idPrimitivanTip;
    }

    public void setIdPrimitivanTip(PrimitivanTip idPrimitivanTip) {
        this.idPrimitivanTip = idPrimitivanTip;
    }

    public TipAtributDomena getIdTipAtributDomena() {
        return idTipAtributDomena;
    }

    public void setIdTipAtributDomena(TipAtributDomena idTipAtributDomena) {
        this.idTipAtributDomena = idTipAtributDomena;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtributDomena != null ? idAtributDomena.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtributDomena)) {
            return false;
        }
        AtributDomena other = (AtributDomena) object;
        if ((this.idAtributDomena == null && other.idAtributDomena != null) || (this.idAtributDomena != null && !this.idAtributDomena.equals(other.idAtributDomena))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.AtributDomena[ idAtributDomena=" + idAtributDomena + " ]";
    }
    
}
