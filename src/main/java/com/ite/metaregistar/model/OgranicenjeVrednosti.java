/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SVETLANA.LOKAL
 */
@Entity
@Table(name = "ogranicenje_vrednosti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OgranicenjeVrednosti.findAll", query = "SELECT o FROM OgranicenjeVrednosti o")
    , @NamedQuery(name = "OgranicenjeVrednosti.findByIdVrstaOsobine", query = "SELECT o FROM OgranicenjeVrednosti o WHERE o.ogranicenjeVrednostiPK.idVrstaOsobine = :idVrstaOsobine")
    , @NamedQuery(name = "OgranicenjeVrednosti.findByIdAtributDomena", query = "SELECT o FROM OgranicenjeVrednosti o WHERE o.ogranicenjeVrednostiPK.idAtributDomena = :idAtributDomena")
    , @NamedQuery(name = "OgranicenjeVrednosti.findByVrednostOsobine", query = "SELECT o FROM OgranicenjeVrednosti o WHERE o.vrednostOsobine = :vrednostOsobine")})
public class OgranicenjeVrednosti implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OgranicenjeVrednostiPK ogranicenjeVrednostiPK;
    @Size(max = 250)
    @Column(name = "vrednostOsobine")
    private String vrednostOsobine;
    @JoinColumn(name = "idAtributDomena", referencedColumnName = "idAtributDomena", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AtributDomena atributDomena;
    @JoinColumn(name = "idVrstaOsobine", referencedColumnName = "idVrstaOsobine", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private VrstaOsobine vrstaOsobine;

    public OgranicenjeVrednosti() {
    }

    public OgranicenjeVrednosti(OgranicenjeVrednostiPK ogranicenjeVrednostiPK) {
        this.ogranicenjeVrednostiPK = ogranicenjeVrednostiPK;
    }

    public OgranicenjeVrednosti(int idVrstaOsobine, int idAtributDomena) {
        this.ogranicenjeVrednostiPK = new OgranicenjeVrednostiPK(idVrstaOsobine, idAtributDomena);
    }

    public OgranicenjeVrednostiPK getOgranicenjeVrednostiPK() {
        return ogranicenjeVrednostiPK;
    }

    public void setOgranicenjeVrednostiPK(OgranicenjeVrednostiPK ogranicenjeVrednostiPK) {
        this.ogranicenjeVrednostiPK = ogranicenjeVrednostiPK;
    }

    public String getVrednostOsobine() {
        return vrednostOsobine;
    }

    public void setVrednostOsobine(String vrednostOsobine) {
        this.vrednostOsobine = vrednostOsobine;
    }

    public AtributDomena getAtributDomena() {
        return atributDomena;
    }

    public void setAtributDomena(AtributDomena atributDomena) {
        this.atributDomena = atributDomena;
    }

    public VrstaOsobine getVrstaOsobine() {
        return vrstaOsobine;
    }

    public void setVrstaOsobine(VrstaOsobine vrstaOsobine) {
        this.vrstaOsobine = vrstaOsobine;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ogranicenjeVrednostiPK != null ? ogranicenjeVrednostiPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OgranicenjeVrednosti)) {
            return false;
        }
        OgranicenjeVrednosti other = (OgranicenjeVrednosti) object;
        if ((this.ogranicenjeVrednostiPK == null && other.ogranicenjeVrednostiPK != null) || (this.ogranicenjeVrednostiPK != null && !this.ogranicenjeVrednostiPK.equals(other.ogranicenjeVrednostiPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.OgranicenjeVrednosti[ ogranicenjeVrednostiPK=" + ogranicenjeVrednostiPK + " ]";
    }
    
}
