/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "korisnik")
@XmlRootElement
public class Korisnik implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idKorisnik")
    private Integer idKorisnik;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "imePrezime")
    private String imePrezime;
    @JoinTable(name = "korisnik_nivo_pristupa", joinColumns = {
        @JoinColumn(name = "idKorisnik", referencedColumnName = "idKorisnik")}, inverseJoinColumns = {
        @JoinColumn(name = "idNivoPristupa", referencedColumnName = "idNivoPristupa")})
    @ManyToMany(fetch = FetchType.EAGER)
    private List<NivoPristupa> nivoPristupaCollection;
    @JoinColumn(name = "idOrgan", referencedColumnName = "idOrgan")
    @ManyToOne
    private Organ idOrgan;
    @OneToMany(mappedBy = "idKorisnik")
    private Collection<Registar> registarCollection;

    public Korisnik() {
    }

    public Korisnik(Integer idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public Integer getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(Integer idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getImePrezime() {
        return imePrezime;
    }

    public void setImePrezime(String imePrezime) {
        this.imePrezime = imePrezime;
    }

    public Organ getIdOrgan() {
        return idOrgan;
    }

    public void setIdOrgan(Organ idOrgan) {
        this.idOrgan = idOrgan;
    }

    @XmlTransient
    public List<NivoPristupa> getNivoPristupaCollection() {
        return nivoPristupaCollection;
    }

    public void setNivoPristupaCollection(List<NivoPristupa> nivoPristupaCollection) {
        this.nivoPristupaCollection = nivoPristupaCollection;
    }
    
    @XmlTransient
    public Collection<Registar> getRegistarCollection() {
        return registarCollection;
    }

    public void setRegistarCollection(Collection<Registar> registarCollection) {
        this.registarCollection = registarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKorisnik != null ? idKorisnik.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Korisnik)) {
            return false;
        }
        Korisnik other = (Korisnik) object;
        if ((this.idKorisnik == null && other.idKorisnik != null) || (this.idKorisnik != null && !this.idKorisnik.equals(other.idKorisnik))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.Korisnik[ idKorisnik=" + idKorisnik + " ]";
    }

}
