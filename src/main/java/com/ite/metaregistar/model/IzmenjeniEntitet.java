/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import com.ite.metaregistar.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "izmenjeni_entitet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IzmenjeniEntitet.findAll", query = "SELECT i FROM IzmenjeniEntitet i")
    , @NamedQuery(name = "IzmenjeniEntitet.findByIdIzmenjeni", query = "SELECT i FROM IzmenjeniEntitet i WHERE i.idIzmenjeni = :idIzmenjeni")
    , @NamedQuery(name = "IzmenjeniEntitet.findByNovaVrednost", query = "SELECT i FROM IzmenjeniEntitet i WHERE i.novaVrednost = :novaVrednost")
    , @NamedQuery(name = "IzmenjeniEntitet.findByIdentifikacionaOznaka", query = "SELECT i FROM IzmenjeniEntitet i WHERE i.identifikacionaOznaka = :identifikacionaOznaka")
    , @NamedQuery(name = "IzmenjeniEntitet.findByNazivPoslovnogDomena", query = "SELECT i FROM IzmenjeniEntitet i WHERE i.nazivPoslovnogDomena = :nazivPoslovnogDomena")})
public class IzmenjeniEntitet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "idIzmenjeni")
    private Integer idIzmenjeni;
    @Size(max = 2147483647)
    @Column(name = "novaVrednost")
    private String novaVrednost;
    @Size(max = 2147483647)
    @Column(name = "identifikacionaOznaka")
    private String identifikacionaOznaka;
    @Size(max = 2147483647)
    @Column(name = "nazivPoslovnogDomena")
    private String nazivPoslovnogDomena;
    @Size(max = 2147483647)
    @Column(name = "vrednostIdentifOznaka")
    private String vrednostIdentifOznaka;
    @Column(name = "datumVreme")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumVreme;

    public IzmenjeniEntitet() {
    }

    public IzmenjeniEntitet(Integer idIzmenjeni) {
        this.idIzmenjeni = idIzmenjeni;
    }

    public IzmenjeniEntitet(String novaVrednost, String identifikacionaOznaka, String nazivPoslovnogDomena, String vrednostIdentifOznaka, Date datumVreme) {
        this.novaVrednost = novaVrednost;
        this.identifikacionaOznaka = identifikacionaOznaka;
        this.nazivPoslovnogDomena = nazivPoslovnogDomena;
        this.vrednostIdentifOznaka = vrednostIdentifOznaka;
        this.datumVreme = datumVreme;
    }

    public IzmenjeniEntitet(String novaVrednost, String identifikacionaOznaka, String nazivPoslovnogDomena, String vrednostIdentifOznaka) {
        this.novaVrednost = novaVrednost;
        this.identifikacionaOznaka = identifikacionaOznaka;
        this.nazivPoslovnogDomena = nazivPoslovnogDomena;
        this.vrednostIdentifOznaka = vrednostIdentifOznaka;
    }

    public IzmenjeniEntitet(String novaVrednost, String identifikacionaOznaka, String nazivPoslovnogDomena) {
        this.novaVrednost = novaVrednost;
        this.identifikacionaOznaka = identifikacionaOznaka;
        this.nazivPoslovnogDomena = nazivPoslovnogDomena;
    }

    public Integer getIdIzmenjeni() {
        return idIzmenjeni;
    }

    public void setIdIzmenjeni(Integer idIzmenjeni) {
        this.idIzmenjeni = idIzmenjeni;
    }

    public String getNovaVrednost() {
        return novaVrednost;
    }

    public void setNovaVrednost(String novaVrednost) {
        this.novaVrednost = novaVrednost;
    }

    public String getIdentifikacionaOznaka() {
        return identifikacionaOznaka;
    }

    public void setIdentifikacionaOznaka(String identifikacionaOznaka) {
        this.identifikacionaOznaka = identifikacionaOznaka;
    }

    public String getNazivPoslovnogDomena() {
        return nazivPoslovnogDomena;
    }

    public void setNazivPoslovnogDomena(String nazivPoslovnogDomena) {
        this.nazivPoslovnogDomena = nazivPoslovnogDomena;
    }

    public String getVrednostIdentifOznaka() {
        return vrednostIdentifOznaka;
    }

    public void setVrednostIdentifOznaka(String vrednostIdentifOznaka) {
        this.vrednostIdentifOznaka = vrednostIdentifOznaka;
    }

    public Date getDatumVreme() {
        return datumVreme;
    }

    public void setDatumVreme(Date datumVreme) {
        this.datumVreme = datumVreme;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIzmenjeni != null ? idIzmenjeni.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IzmenjeniEntitet)) {
            return false;
        }
        IzmenjeniEntitet other = (IzmenjeniEntitet) object;
        if ((this.idIzmenjeni == null && other.idIzmenjeni != null) || (this.idIzmenjeni != null && !this.idIzmenjeni.equals(other.idIzmenjeni))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.test.IzmenjeniEntitet[ idIzmenjeni=" + idIzmenjeni + " ]";
    }

}
