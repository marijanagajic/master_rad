/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "organ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Organ.findAll", query = "SELECT o FROM Organ o")
    , @NamedQuery(name = "Organ.findByIdOrgan", query = "SELECT o FROM Organ o WHERE o.idOrgan = :idOrgan")
    , @NamedQuery(name = "Organ.findByPib", query = "SELECT o FROM Organ o WHERE o.pib = :pib")
    , @NamedQuery(name = "Organ.findByMaticniBroj", query = "SELECT o FROM Organ o WHERE o.maticniBroj = :maticniBroj")
    , @NamedQuery(name = "Organ.findByNazivOrgana", query = "SELECT o FROM Organ o WHERE o.nazivOrgana = :nazivOrgana")})
public class Organ implements Serializable {

    @OneToMany(mappedBy = "idOrgan")
    private Collection<Korisnik> korisnikCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idOrgan")
    private Integer idOrgan;
    @Column(name = "pib")
    private String pib;
    @Column(name = "maticniBroj")
    private String maticniBroj;
    @Column(name = "nazivOrgana")
    private String nazivOrgana;
    @JoinColumn(name = "idTipOrgana", referencedColumnName = "idTipOrgana")
    @ManyToOne
    private TipOrgana idTipOrgana;
    @OneToMany(mappedBy = "idOrgan")
    private Collection<Registar> registarCollection;

    public Organ() {
    }

    public Organ(Integer idOrgan) {
        this.idOrgan = idOrgan;
    }

    public Integer getIdOrgan() {
        return idOrgan;
    }

    public void setIdOrgan(Integer idOrgan) {
        this.idOrgan = idOrgan;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getMaticniBroj() {
        return maticniBroj;
    }

    public void setMaticniBroj(String maticniBroj) {
        this.maticniBroj = maticniBroj;
    }

    public String getNazivOrgana() {
        return nazivOrgana;
    }

    public void setNazivOrgana(String nazivOrgana) {
        this.nazivOrgana = nazivOrgana;
    }

    public TipOrgana getIdTipOrgana() {
        return idTipOrgana;
    }

    public void setIdTipOrgana(TipOrgana idTipOrgana) {
        this.idTipOrgana = idTipOrgana;
    }

    @XmlTransient
    public Collection<Registar> getRegistarCollection() {
        return registarCollection;
    }

    public void setRegistarCollection(Collection<Registar> registarCollection) {
        this.registarCollection = registarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrgan != null ? idOrgan.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organ)) {
            return false;
        }
        Organ other = (Organ) object;
        if ((this.idOrgan == null && other.idOrgan != null) || (this.idOrgan != null && !this.idOrgan.equals(other.idOrgan))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.Organ[ idOrgan=" + idOrgan + " ]";
    }

    @XmlTransient
    public Collection<Korisnik> getKorisnikCollection() {
        return korisnikCollection;
    }

    public void setKorisnikCollection(Collection<Korisnik> korisnikCollection) {
        this.korisnikCollection = korisnikCollection;
    }
    
}
