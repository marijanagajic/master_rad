/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "osnov_nadleznosti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OsnovNadleznosti.findAll", query = "SELECT o FROM OsnovNadleznosti o")
    , @NamedQuery(name = "OsnovNadleznosti.findByIdNadleznost", query = "SELECT o FROM OsnovNadleznosti o WHERE o.idNadleznost = :idNadleznost")})
public class OsnovNadleznosti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idNadleznost")
    private Integer idNadleznost;
    @Column(name = "nazivNadleznost")
    private String nazivNadleznost;
    @OneToMany(mappedBy = "idOsnovNadleznosti")
    private Collection<Registar> registarCollection;

    public OsnovNadleznosti() {
    }

    public OsnovNadleznosti(Integer idNadleznost) {
        this.idNadleznost = idNadleznost;
    }

    public Integer getIdNadleznost() {
        return idNadleznost;
    }

    public void setIdNadleznost(Integer idNadleznost) {
        this.idNadleznost = idNadleznost;
    }

    public String getNazivNadleznost() {
        return nazivNadleznost;
    }

    public void setNazivNadleznost(String nazivNadleznost) {
        this.nazivNadleznost = nazivNadleznost;
    }

    @XmlTransient
    public Collection<Registar> getRegistarCollection() {
        return registarCollection;
    }

    public void setRegistarCollection(Collection<Registar> registarCollection) {
        this.registarCollection = registarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNadleznost != null ? idNadleznost.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OsnovNadleznosti)) {
            return false;
        }
        OsnovNadleznosti other = (OsnovNadleznosti) object;
        if ((this.idNadleznost == null && other.idNadleznost != null) || (this.idNadleznost != null && !this.idNadleznost.equals(other.idNadleznost))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.OsnovNadleznosti[ idNadleznost=" + idNadleznost + " ]";
    }
    
}
