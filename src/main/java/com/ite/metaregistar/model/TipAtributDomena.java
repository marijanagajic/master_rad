/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SVETLANA.LOKAL
 */
@Entity
@Table(name = "tip_atribut_domena")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipAtributDomena.findAll", query = "SELECT t FROM TipAtributDomena t")
    , @NamedQuery(name = "TipAtributDomena.findByIdTipAtributDomena", query = "SELECT t FROM TipAtributDomena t WHERE t.idTipAtributDomena = :idTipAtributDomena")
    , @NamedQuery(name = "TipAtributDomena.findByNazivTipAtributDomena", query = "SELECT t FROM TipAtributDomena t WHERE t.nazivTipAtributDomena = :nazivTipAtributDomena")})
public class TipAtributDomena implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "idTipAtributDomena")
    private Integer idTipAtributDomena;
    @Size(max = 250)
    @Column(name = "nazivTipAtributDomena")
    private String nazivTipAtributDomena;
    @OneToMany(mappedBy = "idTipAtributDomena")
    private Collection<AtributDomena> atributDomenaCollection;

    public TipAtributDomena() {
    }

    public TipAtributDomena(Integer idTipAtributDomena) {
        this.idTipAtributDomena = idTipAtributDomena;
    }

    public Integer getIdTipAtributDomena() {
        return idTipAtributDomena;
    }

    public void setIdTipAtributDomena(Integer idTipAtributDomena) {
        this.idTipAtributDomena = idTipAtributDomena;
    }

    public String getNazivTipAtributDomena() {
        return nazivTipAtributDomena;
    }

    public void setNazivTipAtributDomena(String nazivTipAtributDomena) {
        this.nazivTipAtributDomena = nazivTipAtributDomena;
    }

    @XmlTransient
    public Collection<AtributDomena> getAtributDomenaCollection() {
        return atributDomenaCollection;
    }

    public void setAtributDomenaCollection(Collection<AtributDomena> atributDomenaCollection) {
        this.atributDomenaCollection = atributDomenaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipAtributDomena != null ? idTipAtributDomena.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipAtributDomena)) {
            return false;
        }
        TipAtributDomena other = (TipAtributDomena) object;
        if ((this.idTipAtributDomena == null && other.idTipAtributDomena != null) || (this.idTipAtributDomena != null && !this.idTipAtributDomena.equals(other.idTipAtributDomena))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.TipAtributDomena[ idTipAtributDomena=" + idTipAtributDomena + " ]";
    }
    
}
