/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marijana
 */
@Entity
@Table(name = "nivo_pristupa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivoPristupa.findAll", query = "SELECT n FROM NivoPristupa n")
    , @NamedQuery(name = "NivoPristupa.findByIdNivoPristupa", query = "SELECT n FROM NivoPristupa n WHERE n.idNivoPristupa = :idNivoPristupa")
    , @NamedQuery(name = "NivoPristupa.findByNazivNivoPristupa", query = "SELECT n FROM NivoPristupa n WHERE n.nazivNivoPristupa = :nazivNivoPristupa")
    , @NamedQuery(name = "NivoPristupa.findByOpisNivoPristupa", query = "SELECT n FROM NivoPristupa n WHERE n.opisNivoPristupa = :opisNivoPristupa")})
public class NivoPristupa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idNivoPristupa")
    private Integer idNivoPristupa;
    @Column(name = "nazivNivoPristupa")
    private String nazivNivoPristupa;
    @Column(name = "opisNivoPristupa")
    private String opisNivoPristupa;
    @ManyToMany(mappedBy = "nivoPristupaCollection")
    private Collection<Korisnik> korisnikCollection;

    public NivoPristupa() {
    }

    public NivoPristupa(Integer idNivoPristupa) {
        this.idNivoPristupa = idNivoPristupa;
    }

    public Integer getIdNivoPristupa() {
        return idNivoPristupa;
    }

    public void setIdNivoPristupa(Integer idNivoPristupa) {
        this.idNivoPristupa = idNivoPristupa;
    }

    public String getNazivNivoPristupa() {
        return nazivNivoPristupa;
    }

    public void setNazivNivoPristupa(String nazivNivoPristupa) {
        this.nazivNivoPristupa = nazivNivoPristupa;
    }

    public String getOpisNivoPristupa() {
        return opisNivoPristupa;
    }

    public void setOpisNivoPristupa(String opisNivoPristupa) {
        this.opisNivoPristupa = opisNivoPristupa;
    }

    @XmlTransient
    public Collection<Korisnik> getKorisnikCollection() {
        return korisnikCollection;
    }

    public void setKorisnikCollection(Collection<Korisnik> korisnikCollection) {
        this.korisnikCollection = korisnikCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivoPristupa != null ? idNivoPristupa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivoPristupa)) {
            return false;
        }
        NivoPristupa other = (NivoPristupa) object;
        if ((this.idNivoPristupa == null && other.idNivoPristupa != null) || (this.idNivoPristupa != null && !this.idNivoPristupa.equals(other.idNivoPristupa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ite.metaregistar.model.NivoPristupa[ idNivoPristupa=" + idNivoPristupa + " ]";
    }
    
}
