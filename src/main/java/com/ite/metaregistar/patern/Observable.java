/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.patern;

import com.ite.metaregistar.model.EntitetRegistra;
import com.ite.metaregistar.model.IzmenjeniEntitet;
import com.ite.metaregistar.model.Klijent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marijana
 */
public class Observable {

     IzmenjeniEntitet novaVrednost;
//    String novaVrednost;
    List<Klijent> listaEntiteta;

    public Observable(IzmenjeniEntitet novaVrednost) {
        //this.izmenjeniEntitet = izmenjeniEntitet;
        this.novaVrednost = novaVrednost;
        listaEntiteta = new ArrayList<Klijent>();
    }

    public IzmenjeniEntitet getNovaVrednost() {
        return novaVrednost;
    }

    public void registrujEntitet(EntitetRegistra entitet) {
        listaEntiteta.add(entitet);
    }

    public void promenaVrednosti(IzmenjeniEntitet novaVrednost) {
        this.novaVrednost = novaVrednost;
        obavestiKlijente();
    }

    private void obavestiKlijente() {
        for (Klijent entitet : listaEntiteta) {
            entitet.osvezi();
        }
    }

//    public void promenaVrednosti(IzmenjeniEntitet izmenjeni) {
//        this.izmenjeniEntitet = izmenjeni;
//        obavestiKlijente();
//    }
}
