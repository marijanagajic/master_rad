/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.patern;

import com.ite.metaregistar.model.EntitetRegistra;
import com.ite.metaregistar.service.EntitetRegistraService;
import java.util.List;

/**
 *
 * @author Marijana
 */
public class Factory {

    public Observable kreirajObjekat(String tipObjekta, String nazivDomena, EntitetRegistraService ers) {
        if (tipObjekta.equalsIgnoreCase("Observable")) {
            Observable obs = new Observable(null);
            List<EntitetRegistra> lista = ers.findAllEntitetRegistraNijeIzvorni();
            for (EntitetRegistra entitet : lista) {
                if (entitet.getIdDomen().getNazivDomen().equals(nazivDomena)) {
                    entitet.setObservable(obs);
                    obs.registrujEntitet(entitet);
                }
            }
            return obs;
        }
        return null;
    }
}
