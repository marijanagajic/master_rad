
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.Domen;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface DomenService {
    
    void addDomen(Domen domen);
    void editDomen(Domen domen);
    void deleteDomen(int idDomen);
    Domen findDomen(int idDomen);
    Domen findDomenbyName(String nazivPristupa);
    List<Domen> findAllDomen();
    
}
