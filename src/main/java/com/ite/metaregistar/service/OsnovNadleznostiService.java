
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.OsnovNadleznosti;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface OsnovNadleznostiService {
    
    void addOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti);
    void editOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti);
    void deleteOsnovNadleznosti(int idOsnovNadleznosti);
    OsnovNadleznosti findOsnovNadleznosti(int idOsnovNadleznosti);
    OsnovNadleznosti findOsnovNadleznostibyName(String nazivPristupa);
    List<OsnovNadleznosti> findAllOsnovNadleznosti();
    
}
