
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.TipOrgana;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface TipOrganaService {
    
    void addTipOrgana(TipOrgana tipOrgana);
    void editTipOrgana(TipOrgana tipOrgana);
    void deleteTipOrgana(int idTipOrgana);
    TipOrgana findTipOrgana(int idTipOrgana);
    TipOrgana findTipOrganabyName(String nazivPristupa);
    List<TipOrgana> findAllTipOrgana();
    
}
