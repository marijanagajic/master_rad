
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.EntitetRegistra;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface EntitetRegistraService {
    
    void addEntitetRegistra(EntitetRegistra entitetRegistra);
    void editEntitetRegistra(EntitetRegistra entitetRegistra);
    void deleteEntitetRegistra(int idEntitetRegistra);
    EntitetRegistra findEntitetRegistra(int idEntitetRegistra);
    EntitetRegistra findEntitetRegistrabyName(String nazivPristupa);
    List<EntitetRegistra> findAllEntitetRegistra();
    List<EntitetRegistra> findAllEntitetRegistraNijeIzvorni();
    List<EntitetRegistra> findAllEntitetRegistraByIdRegistar(int idRegistar);
}
