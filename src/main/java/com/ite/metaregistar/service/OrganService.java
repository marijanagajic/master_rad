
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.Organ;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface OrganService {
    
    void addOrgan(Organ organ);
    void editOrgan(Organ organ);
    void deleteOrgan(int idOrgan);
    Organ findOrgan(int idOrgan);
    Organ findOrganbyName(String nazivPristupa);
    List<Organ> findAllOrgan();
    
}
