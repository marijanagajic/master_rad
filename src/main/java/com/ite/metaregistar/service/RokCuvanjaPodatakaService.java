
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.RokCuvanjaPodataka;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface RokCuvanjaPodatakaService {
    
    void addRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka);
    void editRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka);
    void deleteRokCuvanjaPodataka(int idRokCuvanjaPodataka);
    RokCuvanjaPodataka findRokCuvanjaPodataka(int idRokCuvanjaPodataka);
    RokCuvanjaPodataka findRokCuvanjaPodatakabyName(String nazivPristupa);
    List<RokCuvanjaPodataka> findAllRokCuvanjaPodataka();
    
}
