
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.OgranicenjeVrednostiDao;
import com.ite.metaregistar.model.OgranicenjeVrednosti;
import com.ite.metaregistar.model.OgranicenjeVrednostiPK;
import com.ite.metaregistar.service.OgranicenjeVrednostiService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class OgranicenjeVrednostiServiceImpl implements OgranicenjeVrednostiService{
    
   @Autowired
   private OgranicenjeVrednostiDao npd;

    @Override
    public void addOgranicenjeVrednosti(OgranicenjeVrednosti ogranicenjeVrednosti) {
      npd.addOgranicenjeVrednosti(ogranicenjeVrednosti);
    }

    @Override
    public void editOgranicenjeVrednosti(OgranicenjeVrednosti ogranicenjeVrednosti) {
      npd.editOgranicenjeVrednosti(ogranicenjeVrednosti);
    }

    @Override
    public OgranicenjeVrednosti findOgranicenjeVrednosti(OgranicenjeVrednostiPK ogranicenjeVrednostiPK) {
        return npd.findOgranicenjeVrednosti(ogranicenjeVrednostiPK);
    }

    @Override
    public OgranicenjeVrednosti findOgranicenjeVrednostibyName(String nazivOgranicenjeVrednosti) {
        return npd.findOgranicenjeVrednostibyName(nazivOgranicenjeVrednosti);
    }

    @Override
    public List<OgranicenjeVrednosti> findAllOgranicenjeVrednosti() {
          return npd.findAllOgranicenjeVrednostie();
    }
    
}
