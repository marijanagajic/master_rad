
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.TipOrganaDao;
import com.ite.metaregistar.model.TipOrgana;
import com.ite.metaregistar.service.TipOrganaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class TipOrganaServiceImpl implements TipOrganaService{
    
   @Autowired
   private TipOrganaDao npd;

    @Override
    public void addTipOrgana(TipOrgana tipTipOrganaa) {
      npd.addTipOrgana(tipTipOrganaa);
    }

    @Override
    public void editTipOrgana(TipOrgana tipTipOrganaa) {
      npd.editTipOrgana(tipTipOrganaa);
    }

    @Override
    public void deleteTipOrgana(int idTipOrgana) {
       npd.deleteTipOrgana(idTipOrgana);
    }

    @Override
    public TipOrgana findTipOrgana(int idTipOrgana) {
        return npd.findTipOrgana(idTipOrgana);
    }

    @Override
    public TipOrgana findTipOrganabyName(String nazivTipOrgana) {
        return npd.findTipOrganabyName(nazivTipOrgana);
    }

    @Override
    public List<TipOrgana> findAllTipOrgana() {
          return npd.findAllTipOrgana();
    }
    
}
