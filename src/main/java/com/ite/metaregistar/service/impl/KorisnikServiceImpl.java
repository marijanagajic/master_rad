/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.KorisnikDao;
import com.ite.metaregistar.model.Korisnik;
import com.ite.metaregistar.service.KorisnikService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class KorisnikServiceImpl implements KorisnikService{
    
    @Autowired
    private KorisnikDao kd;

    @Override
    public void addKorisnik(Korisnik korisnik){
       kd.addKorisnik(korisnik);
    }

    @Override
    public void editKorisnik(Korisnik korisnik) {
       kd.editKorisnik(korisnik);
    }

    @Override
    public void deleteKorisnik(int idKorisnik) {
        kd.deleteKorisnik(idKorisnik);
    }

    @Override
    public Korisnik findKorisnik(int idKorisnik) {
        return kd.findKorisnik(idKorisnik);
    }

    @Override
    public Korisnik findKorisnikbyName(String username) {
        return kd.findKorisnikbyName(username);
    }

    @Override
    public List<Korisnik> findAllKorisnike() {
        return kd.findAllKorisnike();
    }
    
}
