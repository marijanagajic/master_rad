
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.PrimitivanTipDao;
import com.ite.metaregistar.model.PrimitivanTip;
import com.ite.metaregistar.service.PrimitivanTipService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class PrimitivanTipServiceImpl implements PrimitivanTipService{
    
   @Autowired
   private PrimitivanTipDao npd;

    @Override
    public void addPrimitivanTip(PrimitivanTip primitivanTip) {
      npd.addPrimitivanTip(primitivanTip);
    }

    @Override
    public void editPrimitivanTip(PrimitivanTip primitivanTip) {
      npd.editPrimitivanTip(primitivanTip);
    }

    @Override
    public void deletePrimitivanTip(int idPrimitivanTip) {
       npd.deletePrimitivanTip(idPrimitivanTip);
    }

    @Override
    public PrimitivanTip findPrimitivanTip(int idPrimitivanTip) {
        return npd.findPrimitivanTip(idPrimitivanTip);
    }

    @Override
    public PrimitivanTip findPrimitivanTipbyName(String nazivPrimitivanTip) {
        return npd.findPrimitivanTipbyName(nazivPrimitivanTip);
    }

    @Override
    public List<PrimitivanTip> findAllPrimitivanTip() {
          return npd.findAllPrimitivanTipe();
    }
    
}
