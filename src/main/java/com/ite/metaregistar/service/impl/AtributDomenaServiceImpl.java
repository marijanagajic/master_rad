
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.AtributDomenaDao;
import com.ite.metaregistar.model.AtributDomena;
import com.ite.metaregistar.service.AtributDomenaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class AtributDomenaServiceImpl implements AtributDomenaService{
    
   @Autowired
   private AtributDomenaDao npd;

    @Override
    public void addAtributDomena(AtributDomena atributDomena) {
      npd.addAtributDomena(atributDomena);
    }

    @Override
    public void editAtributDomena(AtributDomena atributDomena) {
      npd.editAtributDomena(atributDomena);
    }

    @Override
    public void deleteAtributDomena(int idAtributDomena) {
       npd.deleteAtributDomena(idAtributDomena);
    }

    @Override
    public AtributDomena findAtributDomena(int idAtributDomena) {
        return npd.findAtributDomena(idAtributDomena);
    }

    @Override
    public AtributDomena findAtributDomenabyName(String nazivAtributDomena) {
        return npd.findAtributDomenabyName(nazivAtributDomena);
    }

    @Override
    public List<AtributDomena> findAllAtributDomena() {
          return npd.findAllAtributDomenae();
    }
    
}
