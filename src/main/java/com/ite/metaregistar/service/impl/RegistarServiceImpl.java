
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.RegistarDao;
import com.ite.metaregistar.model.Registar;
import com.ite.metaregistar.service.RegistarService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class RegistarServiceImpl implements RegistarService{
    
   @Autowired
   private RegistarDao npd;

    @Override
    public void addRegistar(Registar registar) {
      npd.addRegistar(registar);
    }

    @Override
    public void editRegistar(Registar registar) {
      npd.editRegistar(registar);
    }

    @Override
    public void deleteRegistar(int idRegistar) {
       npd.deleteRegistar(idRegistar);
    }

    @Override
    public Registar findRegistar(int idRegistar) {
        return npd.findRegistar(idRegistar);
    }

    @Override
    public Registar findRegistarbyName(String nazivRegistar) {
        return npd.findRegistarbyName(nazivRegistar);
    }

    @Override
    public List<Registar> findAllRegistar() {
          return npd.findAllRegistar();
    }
    
}
