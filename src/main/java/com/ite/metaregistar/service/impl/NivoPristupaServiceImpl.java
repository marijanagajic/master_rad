
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.NivoPristupaDao;
import com.ite.metaregistar.model.NivoPristupa;
import com.ite.metaregistar.service.NivoPristupaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class NivoPristupaServiceImpl implements NivoPristupaService{
    
   @Autowired
   private NivoPristupaDao npd;

    @Override
    public void addNivoPristupa(NivoPristupa nivoPristupa) {
      npd.addNivoPristupa(nivoPristupa);
    }

    @Override
    public void editNivoPristupa(NivoPristupa nivoPristupa) {
      npd.editNivoPristupa(nivoPristupa);
    }

    @Override
    public void deleteNivoPristupa(int idNivoPristupa) {
       npd.deleteNivoPristupa(idNivoPristupa);
    }

    @Override
    public NivoPristupa findNivoPristupa(int idNivoPristupa) {
        return npd.findNivoPristupa(idNivoPristupa);
    }

    @Override
    public NivoPristupa findNazivPristupa(String nazivPristupa) {
        return npd.findNazivPristupa(nazivPristupa);
    }

    @Override
    public List<NivoPristupa> listNivoPristupa() {
          return npd.listNivoPristupa();
    }
    
}
