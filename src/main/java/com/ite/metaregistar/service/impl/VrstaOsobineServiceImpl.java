
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.VrstaOsobineDao;
import com.ite.metaregistar.model.VrstaOsobine;
import com.ite.metaregistar.service.VrstaOsobineService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class VrstaOsobineServiceImpl implements VrstaOsobineService{
    
   @Autowired
   private VrstaOsobineDao npd;

    @Override
    public void addVrstaOsobine(VrstaOsobine vrstaOsobine) {
      npd.addVrstaOsobine(vrstaOsobine);
    }

    @Override
    public void editVrstaOsobine(VrstaOsobine vrstaOsobine) {
      npd.editVrstaOsobine(vrstaOsobine);
    }

    @Override
    public void deleteVrstaOsobine(int idVrstaOsobine) {
       npd.deleteVrstaOsobine(idVrstaOsobine);
    }

    @Override
    public VrstaOsobine findVrstaOsobine(int idVrstaOsobine) {
        return npd.findVrstaOsobine(idVrstaOsobine);
    }

    @Override
    public VrstaOsobine findVrstaOsobinebyName(String nazivVrstaOsobine) {
        return npd.findVrstaOsobinebyName(nazivVrstaOsobine);
    }

    @Override
    public List<VrstaOsobine> findAllVrstaOsobine() {
          return npd.findAllVrstaOsobinee();
    }
    
}
