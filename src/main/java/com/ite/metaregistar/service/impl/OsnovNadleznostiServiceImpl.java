
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.OsnovNadleznostiDao;
import com.ite.metaregistar.model.OsnovNadleznosti;
import com.ite.metaregistar.service.OsnovNadleznostiService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class OsnovNadleznostiServiceImpl implements OsnovNadleznostiService{
    
   @Autowired
   private OsnovNadleznostiDao npd;

    @Override
    public void addOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti) {
      npd.addOsnovNadleznosti(osnovNadleznosti);
    }

    @Override
    public void editOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti) {
      npd.editOsnovNadleznosti(osnovNadleznosti);
    }

    @Override
    public void deleteOsnovNadleznosti(int idOsnovNadleznosti) {
       npd.deleteOsnovNadleznosti(idOsnovNadleznosti);
    }

    @Override
    public OsnovNadleznosti findOsnovNadleznosti(int idOsnovNadleznosti) {
        return npd.findOsnovNadleznosti(idOsnovNadleznosti);
    }

    @Override
    public OsnovNadleznosti findOsnovNadleznostibyName(String nazivOsnovNadleznosti) {
        return npd.findOsnovNadleznostibyName(nazivOsnovNadleznosti);
    }

    @Override
    public List<OsnovNadleznosti> findAllOsnovNadleznosti() {
          return npd.findAllOsnovNadleznostie();
    }
    
}
