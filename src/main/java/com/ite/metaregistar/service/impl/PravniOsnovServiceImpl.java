
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.PravniOsnovDao;
import com.ite.metaregistar.model.PravniOsnov;
import com.ite.metaregistar.service.PravniOsnovService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class PravniOsnovServiceImpl implements PravniOsnovService{
    
   @Autowired
   private PravniOsnovDao npd;

    @Override
    public void addPravniOsnov(PravniOsnov pravniOsnov) {
      npd.addPravniOsnov(pravniOsnov);
    }

    @Override
    public void editPravniOsnov(PravniOsnov pravniOsnov) {
      npd.editPravniOsnov(pravniOsnov);
    }

    @Override
    public void deletePravniOsnov(int idPravniOsnov) {
       npd.deletePravniOsnov(idPravniOsnov);
    }

    @Override
    public PravniOsnov findPravniOsnov(int idPravniOsnov) {
        return npd.findPravniOsnov(idPravniOsnov);
    }

    @Override
    public PravniOsnov findPravniOsnovbyName(String nazivPravniOsnov) {
        return npd.findPravniOsnovbyName(nazivPravniOsnov);
    }

    @Override
    public List<PravniOsnov> findAllPravniOsnov() {
          return npd.findAllPravniOsnove();
    }
    
}
