
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.IzmenjeniEntitetDao;
import com.ite.metaregistar.model.IzmenjeniEntitet;
import com.ite.metaregistar.service.IzmenjeniEntitetService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class IzmenjeniEntitetServiceImpl implements IzmenjeniEntitetService{
    
   @Autowired
   private IzmenjeniEntitetDao ied;

    @Override
    public void addIzmenjeniEntitet(IzmenjeniEntitet izmenjeniEntitet) {
      ied.addIzmenjeniEntitet(izmenjeniEntitet);
    }
    
}
