
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.RokCuvanjaPodatakaDao;
import com.ite.metaregistar.model.RokCuvanjaPodataka;
import com.ite.metaregistar.service.RokCuvanjaPodatakaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class RokCuvanjaPodatakaServiceImpl implements RokCuvanjaPodatakaService{
    
   @Autowired
   private RokCuvanjaPodatakaDao npd;

    @Override
    public void addRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka) {
      npd.addRokCuvanjaPodataka(rokCuvanjaPodataka);
    }

    @Override
    public void editRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka) {
      npd.editRokCuvanjaPodataka(rokCuvanjaPodataka);
    }

    @Override
    public void deleteRokCuvanjaPodataka(int idRokCuvanjaPodataka) {
       npd.deleteRokCuvanjaPodataka(idRokCuvanjaPodataka);
    }

    @Override
    public RokCuvanjaPodataka findRokCuvanjaPodataka(int idRokCuvanjaPodataka) {
        return npd.findRokCuvanjaPodataka(idRokCuvanjaPodataka);
    }

    @Override
    public RokCuvanjaPodataka findRokCuvanjaPodatakabyName(String nazivRokCuvanjaPodataka) {
        return npd.findRokCuvanjaPodatakabyName(nazivRokCuvanjaPodataka);
    }

    @Override
    public List<RokCuvanjaPodataka> findAllRokCuvanjaPodataka() {
          return npd.findAllRokCuvanjaPodataka();
    }
    
}
