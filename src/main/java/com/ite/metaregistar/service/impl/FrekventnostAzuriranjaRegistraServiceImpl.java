
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.FrekventnostAzuriranjaRegistraDao;
import com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra;
import com.ite.metaregistar.service.FrekventnostAzuriranjaRegistraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class FrekventnostAzuriranjaRegistraServiceImpl implements FrekventnostAzuriranjaRegistraService{
    
   @Autowired
   private FrekventnostAzuriranjaRegistraDao npd;

    @Override
    public void addFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistra) {
      npd.addFrekventnostAzuriranjaRegistra(frekventnostAzuriranjaRegistra);
    }

    @Override
    public void editFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistra) {
      npd.editFrekventnostAzuriranjaRegistra(frekventnostAzuriranjaRegistra);
    }

    @Override
    public void deleteFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra) {
       npd.deleteFrekventnostAzuriranjaRegistra(idFrekventnostAzuriranjaRegistra);
    }

    @Override
    public FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra) {
        return npd.findFrekventnostAzuriranjaRegistra(idFrekventnostAzuriranjaRegistra);
    }

    @Override
    public FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistrabyName(String nazivFrekventnostAzuriranjaRegistra) {
        return npd.findFrekventnostAzuriranjaRegistrabyName(nazivFrekventnostAzuriranjaRegistra);
    }

    @Override
    public List<FrekventnostAzuriranjaRegistra> findAllFrekventnostAzuriranjaRegistra() {
          return npd.findAllFrekventnostAzuriranjaRegistra();
    }
    
}
