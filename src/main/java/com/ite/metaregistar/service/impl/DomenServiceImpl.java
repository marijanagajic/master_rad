
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.DomenDao;
import com.ite.metaregistar.model.Domen;
import com.ite.metaregistar.service.DomenService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class DomenServiceImpl implements DomenService{
    
   @Autowired
   private DomenDao npd;

    @Override
    public void addDomen(Domen domen) {
      npd.addDomen(domen);
    }

    @Override
    public void editDomen(Domen domen) {
      npd.editDomen(domen);
    }

    @Override
    public void deleteDomen(int idDomen) {
       npd.deleteDomen(idDomen);
    }

    @Override
    public Domen findDomen(int idDomen) {
        return npd.findDomen(idDomen);
    }

    @Override
    public Domen findDomenbyName(String nazivDomen) {
        return npd.findDomenbyName(nazivDomen);
    }

    @Override
    public List<Domen> findAllDomen() {
          return npd.findAllDomene();
    }
    
}
