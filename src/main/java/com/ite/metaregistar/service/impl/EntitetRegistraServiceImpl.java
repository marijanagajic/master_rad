
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.EntitetRegistraDao;
import com.ite.metaregistar.model.EntitetRegistra;
import com.ite.metaregistar.service.EntitetRegistraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class EntitetRegistraServiceImpl implements EntitetRegistraService{
    
   @Autowired
   private EntitetRegistraDao npd;

    @Override
    public void addEntitetRegistra(EntitetRegistra entitetRegistra) {
      npd.addEntitetRegistra(entitetRegistra);
    }

    @Override
    public void editEntitetRegistra(EntitetRegistra entitetRegistra) {
      npd.editEntitetRegistra(entitetRegistra);
    }

    @Override
    public void deleteEntitetRegistra(int idEntitetRegistra) {
       npd.deleteEntitetRegistra(idEntitetRegistra);
    }

    @Override
    public EntitetRegistra findEntitetRegistra(int idEntitetRegistra) {
        return npd.findEntitetRegistra(idEntitetRegistra);
    }

    @Override
    public EntitetRegistra findEntitetRegistrabyName(String nazivEntitetRegistra) {
        return npd.findEntitetRegistrabyName(nazivEntitetRegistra);
    }

    @Override
    public List<EntitetRegistra> findAllEntitetRegistra() {
          return npd.findAllEntitetRegistra();
    }

    @Override
    public List<EntitetRegistra> findAllEntitetRegistraByIdRegistar(int idRegistar) {
        return npd.findAllEntitetRegistraByIdRegistar(idRegistar);
    }

    @Override
    public List<EntitetRegistra> findAllEntitetRegistraNijeIzvorni() {
        return npd.findAllEntitetRegistraNijeIzvorni();
    }
    
}
