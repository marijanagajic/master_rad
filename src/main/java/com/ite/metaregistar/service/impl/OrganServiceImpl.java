
package com.ite.metaregistar.service.impl;

import com.ite.metaregistar.dao.OrganDao;
import com.ite.metaregistar.model.Organ;
import com.ite.metaregistar.service.OrganService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jelen
 */
@Service
public class OrganServiceImpl implements OrganService{
    
   @Autowired
   private OrganDao npd;

    @Override
    public void addOrgan(Organ organ) {
      npd.addOrgan(organ);
    }

    @Override
    public void editOrgan(Organ organ) {
      npd.editOrgan(organ);
    }

    @Override
    public void deleteOrgan(int idOrgan) {
       npd.deleteOrgan(idOrgan);
    }

    @Override
    public Organ findOrgan(int idOrgan) {
        return npd.findOrgan(idOrgan);
    }

    @Override
    public Organ findOrganbyName(String nazivOrgan) {
        return npd.findOrganbyName(nazivOrgan);
    }

    @Override
    public List<Organ> findAllOrgan() {
          return npd.findAllOrgane();
    }
    
}
