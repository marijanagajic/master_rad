
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.PrimitivanTip;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface PrimitivanTipService {
    
    void addPrimitivanTip(PrimitivanTip primitivanTip);
    void editPrimitivanTip(PrimitivanTip primitivanTip);
    void deletePrimitivanTip(int idPrimitivanTip);
    PrimitivanTip findPrimitivanTip(int idPrimitivanTip);
    PrimitivanTip findPrimitivanTipbyName(String nazivPristupa);
    List<PrimitivanTip> findAllPrimitivanTip();
    
}
