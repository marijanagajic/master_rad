
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface FrekventnostAzuriranjaRegistraService {
    
    void addFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistra);
    void editFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistra);
    void deleteFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra);
    FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra);
    FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistrabyName(String nazivPristupa);
    List<FrekventnostAzuriranjaRegistra> findAllFrekventnostAzuriranjaRegistra();
    
}
