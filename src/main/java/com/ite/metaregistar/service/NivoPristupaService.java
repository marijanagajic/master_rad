
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.NivoPristupa;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface NivoPristupaService {
    
    void addNivoPristupa(NivoPristupa nivoPristupa);
    void editNivoPristupa(NivoPristupa nivoPristupa);
    void deleteNivoPristupa(int idNivoPristupa);
    NivoPristupa findNivoPristupa(int idNivoPristupa);
    NivoPristupa findNazivPristupa(String nazivPristupa);
    List<NivoPristupa> listNivoPristupa();
    
}
