
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.AtributDomena;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface AtributDomenaService {
    
    void addAtributDomena(AtributDomena atributDomena);
    void editAtributDomena(AtributDomena atributDomena);
    void deleteAtributDomena(int idAtributDomena);
    AtributDomena findAtributDomena(int idAtributDomena);
    AtributDomena findAtributDomenabyName(String nazivAtributDomena);
    List<AtributDomena> findAllAtributDomena();
    
}
