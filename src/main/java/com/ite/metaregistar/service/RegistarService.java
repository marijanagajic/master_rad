
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.Registar;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface RegistarService {
    
    void addRegistar(Registar registar);
    void editRegistar(Registar registar);
    void deleteRegistar(int idRegistar);
    Registar findRegistar(int idRegistar);
    Registar findRegistarbyName(String nazivREgistra);
    List<Registar> findAllRegistar();
    
}
