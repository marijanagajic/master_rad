
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.PravniOsnov;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface PravniOsnovService {
    
    void addPravniOsnov(PravniOsnov pravniOsnov);
    void editPravniOsnov(PravniOsnov pravniOsnov);
    void deletePravniOsnov(int idPravniOsnov);
    PravniOsnov findPravniOsnov(int idPravniOsnov);
    PravniOsnov findPravniOsnovbyName(String nazivPristupa);
    List<PravniOsnov> findAllPravniOsnov();
    
}
