
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.IzmenjeniEntitet;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface IzmenjeniEntitetService {
    
    void addIzmenjeniEntitet(IzmenjeniEntitet izmenjeniEntitet);

    
}
