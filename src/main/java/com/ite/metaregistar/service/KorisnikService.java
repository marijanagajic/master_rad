/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.service;

import com.ite.metaregistar.model.Korisnik;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface KorisnikService {
   
    void addKorisnik(Korisnik korisnik);
    void editKorisnik(Korisnik korisnik);
    void deleteKorisnik(int idKorisnik);
    Korisnik findKorisnik(int idKorisnik);
    Korisnik findKorisnikbyName(String username);
    List<Korisnik> findAllKorisnike();
}
