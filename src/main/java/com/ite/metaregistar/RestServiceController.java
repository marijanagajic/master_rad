/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
//import rs.telenor.kolekcija.Kolekcija;
import com.ite.metaregistar.model.EntitetRegistra;
import com.ite.metaregistar.model.IzmenjeniEntitet;
import com.ite.metaregistar.patern.Factory;
import com.ite.metaregistar.patern.Observable;
import com.ite.metaregistar.service.EntitetRegistraService;
import com.ite.metaregistar.service.IzmenjeniEntitetService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Korisnik
 */
@RestController
@RequestMapping("/entitet")
public class RestServiceController {

    @Autowired
    private EntitetRegistraService ers;
    
    @Autowired
    private IzmenjeniEntitetService ies;

    @RequestMapping(value = "/notify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<IzmenjeniEntitet> sendInfo(@RequestBody IzmenjeniEntitet izmenjeni) {
        try {
            String nazivDomena = izmenjeni.getNazivPoslovnogDomena();
            String novaVrednost = izmenjeni.getNovaVrednost();
            
            //factory i observer pattern
            Factory factory = new Factory();
            Observable obs = factory.kreirajObjekat("Observable",nazivDomena, ers);
            
            izmenjeni.setDatumVreme(new Date());
            ies.addIzmenjeniEntitet(izmenjeni);
            obs.promenaVrednosti(izmenjeni);
            return new ResponseEntity<IzmenjeniEntitet>(izmenjeni, HttpStatus.OK);
        } catch (RuntimeException re) {
            return new ResponseEntity<IzmenjeniEntitet>(izmenjeni, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    
    
    
    
    
    
//    @RequestMapping(value = "/notify2", method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<String> sendInfo1(@RequestBody IzmenjeniEntitet izmenjen) {
//        IzmenjeniEntitet izmenjeni = izmenjen;
//        String nazivDomena = izmenjeni.getNazivPoslovnogDomena();
//        // jsonObj.get("identifikacionaOznaka").toString();
//        String novaVrednost = izmenjeni.getNovaVrednost();
////        String nazivDomena = jsonObj.get("nazivPoslovnogDomena").toString();
////        jsonObj.get("identifikacionaOznaka").toString();
////        String novaVrednost = jsonObj.get("novaVrednost").toString();
//        Observable obs = new Observable(null);
//        List<EntitetRegistra> lista = ers.findAllEntitetRegistra();
//        for (EntitetRegistra entitet : lista) {
//            if (entitet.getNijeIzvorniRegistar() == true && entitet.getIdDomen().getNazivDomen().equals(nazivDomena)) {
//                obs.registrujEntitet(entitet);
//            }
//        }
//        ies.addIzmenjeniEntitet(izmenjeni);
//        obs.promenaVrednosti(novaVrednost);
//        return new ResponseEntity<String>(HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/notify1", method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = "{accept:application/json}")
//    public @ResponseBody
//    IzmenjeniEntitet sendInfo(@RequestBody IzmenjeniEntitet izmenjen) {
//        IzmenjeniEntitet izmenjeni = izmenjen;
//        String nazivDomena = izmenjeni.getNazivPoslovnogDomena();
//        // jsonObj.get("identifikacionaOznaka").toString();
//        String novaVrednost = izmenjeni.getNovaVrednost();
////        String nazivDomena = jsonObj.get("nazivPoslovnogDomena").toString();
////        jsonObj.get("identifikacionaOznaka").toString();
////        String novaVrednost = jsonObj.get("novaVrednost").toString();
//        Observable obs = new Observable(null);
//        List<EntitetRegistra> lista = ers.findAllEntitetRegistra();
//        for (EntitetRegistra entitet : lista) {
//            if (entitet.getNijeIzvorniRegistar() == true && entitet.getIdDomen().getNazivDomen() == nazivDomena) {
//                obs.registrujEntitet(entitet);
//            }
//        }
//        ies.addIzmenjeniEntitet(izmenjeni);
//        obs.promenaVrednosti(novaVrednost);
//        return izmenjeni;
//    }
//

}
