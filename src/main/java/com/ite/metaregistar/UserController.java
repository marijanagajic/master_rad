package com.ite.metaregistar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ite.metaregistar.model.AtributDomena;
import com.ite.metaregistar.model.Domen;
import com.ite.metaregistar.model.EntitetRegistra;
import com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra;
import com.ite.metaregistar.model.IzmenjeniEntitet;
import com.ite.metaregistar.model.Korisnik;
import com.ite.metaregistar.model.OgranicenjeVrednosti;
import com.ite.metaregistar.model.OsnovNadleznosti;
import com.ite.metaregistar.model.PravniOsnov;
import com.ite.metaregistar.model.PrimitivanTip;
import com.ite.metaregistar.model.Registar;
import com.ite.metaregistar.model.RokCuvanjaPodataka;
import com.ite.metaregistar.model.TipAtributDomena;
import com.ite.metaregistar.model.TipDomena;
import com.ite.metaregistar.model.VrstaOsobine;
import com.ite.metaregistar.service.AtributDomenaService;
import com.ite.metaregistar.service.DomenService;
import com.ite.metaregistar.service.EntitetRegistraService;
import com.ite.metaregistar.service.FrekventnostAzuriranjaRegistraService;
import com.ite.metaregistar.service.KorisnikService;
import com.ite.metaregistar.service.OgranicenjeVrednostiService;
import com.ite.metaregistar.service.OrganService;
import com.ite.metaregistar.service.OsnovNadleznostiService;
import com.ite.metaregistar.service.PravniOsnovService;
import com.ite.metaregistar.service.PrimitivanTipService;
import com.ite.metaregistar.service.RegistarService;
import com.ite.metaregistar.service.RokCuvanjaPodatakaService;
import java.security.Principal;
import java.util.Locale;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private RegistarService rs;
    @Autowired
    private FrekventnostAzuriranjaRegistraService fas;
    @Autowired
    private OsnovNadleznostiService ons;
    @Autowired
    private PravniOsnovService pos;
    @Autowired
    private RokCuvanjaPodatakaService rcps;
    @Autowired
    private DomenService ds;
    @Autowired
    private EntitetRegistraService ers;
    @Autowired
    private KorisnikService ks;
    @Autowired
    private PrimitivanTipService pts;
    @Autowired
    private AtributDomenaService ads;
    @Autowired
    private OgranicenjeVrednostiService ovs;

    private int porukaInsertRegistar = 0;
    private int porukaUpdateRegistar = 0;

    @RequestMapping(value = "/")
    public String user(Locale locale, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String username = principal.getName();
        model.addAttribute("username", username);
        session.setAttribute("korisnik", ks.findKorisnikbyName(principal.getName()));

        model.addAttribute("poruka", porukaInsertRegistar);
        porukaInsertRegistar = 0;
        model.addAttribute("listaRegistri", rs.findAllRegistar());
        return "user";
    }

    @RequestMapping(value = "/registar")
    public String registar(Model model) {
        model.addAttribute("listaFrekventnostAzuriranja", fas.findAllFrekventnostAzuriranjaRegistra());
        model.addAttribute("listaOsnovNadleznosti", ons.findAllOsnovNadleznosti());
        model.addAttribute("listPravniOsnov", pos.findAllPravniOsnov());
        model.addAttribute("listRokCuvanjaPodataka", rcps.findAllRokCuvanjaPodataka());
        model.addAttribute("listaDomen", ds.findAllDomen());
        com.google.gson.JsonArray array = new com.google.gson.JsonArray();
        for (EntitetRegistra e : ers.findAllEntitetRegistra()) {
            com.google.gson.JsonObject en = new com.google.gson.JsonObject();
            en.addProperty("idEntitet", e.getIdEntitetRegistra());
            en.addProperty("nazivEntitet", e.getNazivEntitetRegistra());
            en.addProperty("idRegistar", e.getIdRegistar().getIdRegistar());
            en.addProperty("idDomen", e.getIdDomen().getIdDomen());
            en.addProperty("izvorni", e.getIzvorniRegistar());
            array.add(en);
        }
        model.addAttribute("entitetiJson", array.toString());

        com.google.gson.JsonArray arrayDomenTip = new com.google.gson.JsonArray();
        for (Domen d : ds.findAllDomen()) {
            com.google.gson.JsonObject domen = new com.google.gson.JsonObject();
            domen.addProperty("idDomen", d.getIdDomen());
            for (AtributDomena atributDomena : d.getAtributDomenaCollection()) {
                if (atributDomena.getIdTipAtributDomena().getIdTipAtributDomena() == 1) {
                    domen.addProperty("idPrimitivanTip", atributDomena.getIdPrimitivanTip().getIdPrimitivanTip());
                }
            }
            arrayDomenTip.add(domen);
        }
        model.addAttribute("domenTipJson", arrayDomenTip.toString());

        com.google.gson.JsonArray arrayPrimTip = new com.google.gson.JsonArray();
        for (PrimitivanTip pt : pts.findAllPrimitivanTip()) {
            com.google.gson.JsonObject ptip = new com.google.gson.JsonObject();
            ptip.addProperty("idPrimitivanTip", pt.getIdPrimitivanTip());
            ptip.addProperty("nazivPrimitivanTip", pt.getNazivPrimitivanTip());
            com.google.gson.JsonArray arrayPrimTipOsobine = new com.google.gson.JsonArray();
            for (VrstaOsobine vrstaOsobine : pt.getVrstaOsobineCollection()) {
                com.google.gson.JsonObject ptipO = new com.google.gson.JsonObject();
                ptipO.addProperty("idVrstaOsobine", vrstaOsobine.getIdVrstaOsobine());
                ptipO.addProperty("nazivVrstaOsobine", vrstaOsobine.getNazivVrstaOsobine());
                arrayPrimTipOsobine.add(ptipO);
            }
            ptip.addProperty("osobine", arrayPrimTipOsobine.toString());
            arrayPrimTip.add(ptip);

        }
        model.addAttribute("listaPrimTipOsobine", arrayPrimTip.toString());

        return "insertRegistar";
    }

    @RequestMapping(value = "/registar/insert", method = RequestMethod.POST)
    public String insertRegistar(@RequestParam(value = "nazivRegistra") String nazivRegistra, @RequestParam(value = "datumUspostavljanja") String datumUspostavljanja,
            @RequestParam(value = "frekventnostAR") int frekventnostAR, @RequestParam(value = "url") String url,
            @RequestParam(value = "osnovNadleznosti") int osnovNadleznosti, @RequestParam(value = "pravniOsnov") int pravniOsnov, @RequestParam(value = "propisNevazeci") String nevazeciPO,
            @RequestParam(value = "rokCuvanjPodataka") int rokCuvanjPodataka, @RequestParam(value = "napomena") String napomena, @RequestParam(value = "vazeci") int vazeci,
            @RequestParam(value = "checkElektronski") int check, @RequestParam(value = "podaciZaInsertJson") String jsonPodaci, Model model, HttpSession session) {

        Registar reg = new Registar();
        com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
        com.google.gson.JsonArray jsonArrayEntiteti = null;
        if (jsonPodaci != "") {
            jsonArrayEntiteti = (com.google.gson.JsonArray) jsonParser.parse(jsonPodaci);
        }

        reg.setNazivRegistar(nazivRegistra);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date sqlDatum = null;
        try {
            sqlDatum = sdf.parse(datumUspostavljanja);
            reg.setDatumUspostavljanja(sqlDatum);
            Korisnik korisnik = (Korisnik) session.getAttribute("korisnik");
            reg.setIdOrgan(korisnik.getIdOrgan());
            reg.setIdFrekventnostAzuriranjaRegistra(new FrekventnostAzuriranjaRegistra(frekventnostAR));
            reg.setIdOsnovNadleznosti(new OsnovNadleznosti(osnovNadleznosti));
            reg.setUrl(url);
            if (vazeci == 1) {
                reg.setIdPravniOsnov(new PravniOsnov(pravniOsnov));
            } else {
                reg.setPropisNevazeci(nevazeciPO);
            }
            reg.setIdRokCuvanjaPodataka(new RokCuvanjaPodataka(rokCuvanjPodataka));
            reg.setNapomena(napomena);
            reg.setDatumUnosa(new Date());
            reg.setIdKorisnik(korisnik);
            reg.setUElektronskomObliku(((int) check == 1) ? (boolean) true : (boolean) false);
            rs.addRegistar(reg);
            if (jsonArrayEntiteti != null) {
                for (JsonElement jsonElement : jsonArrayEntiteti) {
                    if (Boolean.valueOf(jsonElement.getAsJsonObject().get("izvorni").toString().replace("\"", ""))) {
                        Domen poslovni = new Domen();
                        poslovni.setIdTipDomena(new TipDomena(2));
                        poslovni.setIdDomenBaziranNa(new Domen(Integer.parseInt(jsonElement.getAsJsonObject().get("idSemantickiDomen").toString().replace("\"", ""))));
                        poslovni.setNazivDomen(jsonElement.getAsJsonObject().get("nazivPoslovniDomen").toString().replace("\"", ""));
                        ds.addDomen(poslovni);
                        AtributDomena ad = new AtributDomena();
                        ad.setIdDomen(poslovni);
                        ad.setIdTipAtributDomena(new TipAtributDomena(1));
                        ad.setNazivAtributDomena(jsonElement.getAsJsonObject().get("nazivAtributDomena").toString().replace("\"", ""));
                        ad.setIdPrimitivanTip(new PrimitivanTip(Integer.parseInt(jsonElement.getAsJsonObject().get("idPrimitivanTip").toString().replace("\"", ""))));
                        ads.addAtributDomena(ad);
                        com.google.gson.JsonArray jsonArrayOgranicenja = jsonElement.getAsJsonObject().getAsJsonArray("ogranicenjaPDomena");
                        for (JsonElement jsonE : jsonElement.getAsJsonObject().getAsJsonArray("ogranicenjaPDomena")) {
                            OgranicenjeVrednosti ov = new OgranicenjeVrednosti(Integer.parseInt(jsonE.getAsJsonObject().get("idVrstaOsobine").toString().replace("\"", "")), ad.getIdAtributDomena());
                            ov.setVrednostOsobine(jsonE.getAsJsonObject().get("vrednostOgranicenja").toString().replace("\"", ""));
                            ovs.addOgranicenjeVrednosti(ov);
                        }
                        EntitetRegistra e = new EntitetRegistra();
                        e.setIdDomen(poslovni);
                        e.setIzvorniRegistar(Boolean.TRUE);
                        e.setNazivEntitetRegistra(jsonElement.getAsJsonObject().get("nazivEntitet").toString().replace("\"", ""));
                        e.setNazivTabele(jsonElement.getAsJsonObject().get("nazivTabele").toString().replace("\"", ""));
                        e.setIdRegistar(reg);
                        e.setNapomena(jsonElement.getAsJsonObject().get("napomena").toString().replace("\"", ""));
                        ers.addEntitetRegistra(e);
                    } else {
                        EntitetRegistra e = new EntitetRegistra();
                        e.setIdDomen(new Domen(Integer.parseInt(jsonElement.getAsJsonObject().get("idDomen").toString().replace("\"", ""))));
                        e.setIzvorniRegistar(Boolean.FALSE);
                        e.setNazivEntitetRegistra(jsonElement.getAsJsonObject().get("nazivEntitet").toString().replace("\"", ""));
                        e.setIdRegistar(reg);
                        e.setNapomena(jsonElement.getAsJsonObject().get("napomena").toString().replace("\"", ""));
                        ers.addEntitetRegistra(e);
                    }
                }
            }
            porukaInsertRegistar = 1;
        } catch (ParseException ex) {
            porukaInsertRegistar = 2;
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            porukaInsertRegistar = 3;
        }

        return "redirect:/user/";
    }

    @RequestMapping(value = "/registar/pregled/{id}")
    public String registarPregled(@PathVariable(value = "id") int idRegistar, Model model) {
        model.addAttribute("registar", rs.findRegistar(idRegistar));
        model.addAttribute("listaFrekventnostAzuriranja", fas.findAllFrekventnostAzuriranjaRegistra());
        model.addAttribute("listaOsnovNadleznosti", ons.findAllOsnovNadleznosti());
        model.addAttribute("listPravniOsnov", pos.findAllPravniOsnov());
        model.addAttribute("listRokCuvanjaPodataka", rcps.findAllRokCuvanjaPodataka());
        model.addAttribute("listaDomen", ds.findAllDomen());
        com.google.gson.JsonArray array = new com.google.gson.JsonArray();
        for (EntitetRegistra e : ers.findAllEntitetRegistra()) {
            com.google.gson.JsonObject en = new com.google.gson.JsonObject();
            en.addProperty("idEntitet", e.getIdEntitetRegistra());
            en.addProperty("nazivEntitet", e.getNazivEntitetRegistra());
            en.addProperty("idRegistar", e.getIdRegistar().getIdRegistar());
            en.addProperty("idDomen", e.getIdDomen().getIdDomen());
            en.addProperty("izvorni", e.getIzvorniRegistar());
            array.add(en);
        }
        model.addAttribute("entitetiJson", array.toString());

        com.google.gson.JsonArray arrayDomenTip = new com.google.gson.JsonArray();
        for (Domen d : ds.findAllDomen()) {
            com.google.gson.JsonObject domen = new com.google.gson.JsonObject();
            domen.addProperty("idDomen", d.getIdDomen());
            for (AtributDomena atributDomena : d.getAtributDomenaCollection()) {
                if (atributDomena.getIdTipAtributDomena().getIdTipAtributDomena() == 1) {
                    domen.addProperty("idPrimitivanTip", atributDomena.getIdPrimitivanTip().getIdPrimitivanTip());
                }
            }
            arrayDomenTip.add(domen);
        }
        model.addAttribute("domenTipJson", arrayDomenTip.toString());

        com.google.gson.JsonArray arrayPrimTip = new com.google.gson.JsonArray();
        for (PrimitivanTip pt : pts.findAllPrimitivanTip()) {
            com.google.gson.JsonObject ptip = new com.google.gson.JsonObject();
            ptip.addProperty("idPrimitivanTip", pt.getIdPrimitivanTip());
            ptip.addProperty("nazivPrimitivanTip", pt.getNazivPrimitivanTip());
            com.google.gson.JsonArray arrayPrimTipOsobine = new com.google.gson.JsonArray();
            for (VrstaOsobine vrstaOsobine : pt.getVrstaOsobineCollection()) {
                com.google.gson.JsonObject ptipO = new com.google.gson.JsonObject();
                ptipO.addProperty("idVrstaOsobine", vrstaOsobine.getIdVrstaOsobine());
                ptipO.addProperty("nazivVrstaOsobine", vrstaOsobine.getNazivVrstaOsobine());
                arrayPrimTipOsobine.add(ptipO);
            }
            ptip.addProperty("osobine", arrayPrimTipOsobine.toString());
            arrayPrimTip.add(ptip);

        }
        model.addAttribute("listaPrimTipOsobine", arrayPrimTip.toString());
        model.addAttribute("poruka", porukaUpdateRegistar);
        porukaUpdateRegistar = 0;

        return "updateRegistar";
    }

    @RequestMapping(value = "/registar/update", method = RequestMethod.POST)
    public String updateRegistar(@RequestParam(value = "idRegistar") int idRegistar, @RequestParam(value = "nazivRegistra") String nazivRegistra, @RequestParam(value = "datumUspostavljanja") String datumUspostavljanja,
            @RequestParam(value = "frekventnostAR") int frekventnostAR, @RequestParam(value = "osnovNadleznosti") int osnovNadleznosti, @RequestParam(value = "pravniOsnov") int pravniOsnov,
            @RequestParam(value = "propisNevazeci") String nevazeciPO, @RequestParam(value = "url") String url,
            @RequestParam(value = "rokCuvanjPodataka") int rokCuvanjPodataka, @RequestParam(value = "napomena") String napomena, @RequestParam(value = "vazeci") int vazeci,
            @RequestParam(value = "checkElektronski") int check, @RequestParam(value = "podaciZaInsertJson") String jsonPodaci, Model model, HttpSession session) {

        com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
        com.google.gson.JsonArray jsonArrayEntiteti = null;
        if (jsonPodaci != "") {
            jsonArrayEntiteti = (com.google.gson.JsonArray) jsonParser.parse(jsonPodaci);
        }
        Registar reg = rs.findRegistar(idRegistar);
        reg.setNazivRegistar(nazivRegistra);
        reg.setUrl(url);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date sqlDatum = null;
        try {
            sqlDatum = sdf.parse(datumUspostavljanja);
            reg.setDatumUspostavljanja(sqlDatum);
            Korisnik korisnik = (Korisnik) session.getAttribute("korisnik");
            reg.setIdOrgan(korisnik.getIdOrgan());
            reg.setIdFrekventnostAzuriranjaRegistra(new FrekventnostAzuriranjaRegistra(frekventnostAR));
            reg.setIdOsnovNadleznosti(new OsnovNadleznosti(osnovNadleznosti));
            if (vazeci == 1) {
                reg.setIdPravniOsnov(new PravniOsnov(pravniOsnov));
            } else {
                reg.setPropisNevazeci(nevazeciPO);
            }
            reg.setIdRokCuvanjaPodataka(new RokCuvanjaPodataka(rokCuvanjPodataka));
            reg.setNapomena(napomena);
            reg.setDatumUnosa(new Date());
            reg.setIdKorisnik(korisnik);
            reg.setUElektronskomObliku(((int) check == 1) ? (boolean) true : (boolean) false);
            rs.editRegistar(reg);
            if (jsonArrayEntiteti != null) {
                for (JsonElement jsonElement : jsonArrayEntiteti) {
                    if (Boolean.valueOf(jsonElement.getAsJsonObject().get("izvorni").toString().replace("\"", ""))) {
                        Domen poslovni = new Domen();
                        poslovni.setIdTipDomena(new TipDomena(2));
                        poslovni.setIdDomenBaziranNa(new Domen(Integer.parseInt(jsonElement.getAsJsonObject().get("idSemantickiDomen").toString().replace("\"", ""))));
                        poslovni.setNazivDomen(jsonElement.getAsJsonObject().get("nazivPoslovniDomen").toString().replace("\"", ""));
                        ds.addDomen(poslovni);
                        AtributDomena ad = new AtributDomena();
                        ad.setIdDomen(poslovni);
                        ad.setIdTipAtributDomena(new TipAtributDomena(1));
                        ad.setNazivAtributDomena(jsonElement.getAsJsonObject().get("nazivAtributDomena").toString().replace("\"", ""));
                        ad.setIdPrimitivanTip(new PrimitivanTip(Integer.parseInt(jsonElement.getAsJsonObject().get("idPrimitivanTip").toString().replace("\"", ""))));
                        ads.addAtributDomena(ad);
                        com.google.gson.JsonArray jsonArrayOgranicenja = jsonElement.getAsJsonObject().getAsJsonArray("ogranicenjaPDomena");
                        for (JsonElement jsonE : jsonElement.getAsJsonObject().getAsJsonArray("ogranicenjaPDomena")) {
                            OgranicenjeVrednosti ov = new OgranicenjeVrednosti(Integer.parseInt(jsonE.getAsJsonObject().get("idVrstaOsobine").toString().replace("\"", "")), ad.getIdAtributDomena());
                            ov.setVrednostOsobine(jsonE.getAsJsonObject().get("vrednostOgranicenja").toString().replace("\"", ""));
                            ovs.addOgranicenjeVrednosti(ov);
                        }
                        EntitetRegistra e = new EntitetRegistra();
                        e.setIdDomen(poslovni);
                        e.setIzvorniRegistar(Boolean.TRUE);
                        e.setNazivEntitetRegistra(jsonElement.getAsJsonObject().get("nazivEntitet").toString().replace("\"", ""));
                        e.setNazivTabele(jsonElement.getAsJsonObject().get("nazivTabele").toString().replace("\"", ""));
                        e.setIdRegistar(reg);
                        e.setNapomena(jsonElement.getAsJsonObject().get("napomena").toString().replace("\"", ""));
                        ers.addEntitetRegistra(e);
                    } else {
                        EntitetRegistra e = new EntitetRegistra();
                        e.setIdDomen(new Domen(Integer.parseInt(jsonElement.getAsJsonObject().get("idDomen").toString().replace("\"", ""))));
                        e.setIzvorniRegistar(Boolean.FALSE);
                        e.setNazivEntitetRegistra(jsonElement.getAsJsonObject().get("nazivEntitet").toString().replace("\"", ""));
                        e.setIdRegistar(reg);
                        e.setNapomena(jsonElement.getAsJsonObject().get("napomena").toString().replace("\"", ""));
                        ers.addEntitetRegistra(e);
                    }
                }
            }
            porukaUpdateRegistar = 1;
        } catch (ParseException ex) {
            porukaUpdateRegistar = 2;
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            porukaUpdateRegistar = 3;
        }

        return "redirect:/user/registar/pregled/" + String.valueOf(idRegistar);
    }

    @RequestMapping(value = "/promena")
    public String posaljiObavestenje(Model model) {
        List<Domen> domeni = ds.findAllDomen();
        List<Domen> poslovni = new LinkedList<Domen>();
        for (Domen domen : domeni) {
            if (domen.getIdTipDomena().getIdTipDomena() == 2) {
                poslovni.add(domen);
            }
        }
        model.addAttribute("poslovniDomenLista", poslovni);
        return "sendInfo";
    }

    @RequestMapping(value = "/sendInfo", method = RequestMethod.POST)
    public String posaljiObavestenje(@RequestParam(value = "novaVrednost") String novaVrednost, @RequestParam(value = "identO") String identifikacionaOznaka,
            @RequestParam(value = "vrednostIdentO") String vrednostIdentifOznake, @RequestParam(value = "idDomen") int idDomen, RedirectAttributes redir) {
        final String uri = "http://localhost:8084/entitet/notify";
        try {
            Domen domen = ds.findDomen(idDomen);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            IzmenjeniEntitet izm = new IzmenjeniEntitet(novaVrednost, identifikacionaOznaka, domen.getNazivDomen(), vrednostIdentifOznake);
            HttpEntity<IzmenjeniEntitet> request = new HttpEntity<IzmenjeniEntitet>(izm, headers);

            RestTemplate restTemplate = new RestTemplate();
            IzmenjeniEntitet response = restTemplate.postForObject(uri, request, IzmenjeniEntitet.class);
            redir.addFlashAttribute("poruka", "Систем је успешно обавестио регистре о промени");
        } catch (Exception e) {
            redir.addFlashAttribute("poruka", "Грешка: Систем није обавестио регистре о промени");
        }

        //model.addAttribute("json", response.toString());
        return "redirect:/user/promena";
    }
}
