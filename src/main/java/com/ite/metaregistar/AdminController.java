package com.ite.metaregistar;

import com.ite.metaregistar.model.Korisnik;
import com.ite.metaregistar.model.NivoPristupa;
import com.ite.metaregistar.model.Organ;
import com.ite.metaregistar.model.TipOrgana;
import com.ite.metaregistar.service.KorisnikService;
import com.ite.metaregistar.service.NivoPristupaService;
import com.ite.metaregistar.service.OrganService;
import com.ite.metaregistar.service.TipOrganaService;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private KorisnikService ks;
    @Autowired
    private OrganService os;
    @Autowired
    private NivoPristupaService nps;
    @Autowired
    private TipOrganaService tos;

    private int porukaInsertKorisnik = 0;
    private int porukaInsertOrgan = 0;

    @RequestMapping(value = "/")
    public String admin(Locale locale, Model model, Principal principal, HttpSession session) {
        session.setAttribute("korisnik", ks.findKorisnikbyName(principal.getName()));

        return "homeadmin";

    }

    @RequestMapping(value = "/korisnik")
    public String korisnik(Model model) {
        model.addAttribute("listaOrgani", os.findAllOrgan());
        model.addAttribute("listaNivoPristupa", nps.listNivoPristupa());
        model.addAttribute("listaKorisnici", ks.findAllKorisnike());
        model.addAttribute("porukaK", porukaInsertKorisnik);
        model.addAttribute("porukaO", porukaInsertOrgan);
        model.addAttribute("listaTipOrgana", tos.findAllTipOrgana());
        porukaInsertKorisnik = 0;
        porukaInsertOrgan = 0;
        return "insertKorisnik";
    }

    @RequestMapping(value = "/korisnik/insert", method = RequestMethod.POST)
    public String insertKorisnik(@RequestParam(value = "imePrezime") String imePrezime, @RequestParam(value = "username") String username, @RequestParam(value = "password") String password,
            @RequestParam(value = "idOrgan") int idOrgan, @RequestParam(value = "idNivoPristupa") int idNivoPristupa) {
        try {
            Korisnik k = new Korisnik();
            k.setImePrezime(imePrezime);
            k.setUsername(username);
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(password);
            k.setPassword(hashedPassword);
            k.setStatus(Boolean.TRUE);
            k.setIdOrgan(new Organ(idOrgan));
            List<NivoPristupa> nivo = new ArrayList<NivoPristupa>();
            nivo.add(new NivoPristupa(idNivoPristupa));
            k.setNivoPristupaCollection(nivo);
            ks.addKorisnik(k);
            porukaInsertKorisnik = 1;
        } catch (Exception ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
            porukaInsertKorisnik = 2;
        }

        return "redirect:/admin/korisnik";
    }

    @RequestMapping(value = "/organ/insert", method = RequestMethod.POST)
    public String insertOrgan(@RequestParam(value = "nazivOrgana") String nazivOrgana, @RequestParam(value = "pib") String pib, @RequestParam(value = "mb") String mp,
            @RequestParam(value = "idTipOrgana") int idTipOrgan) {
        try {
            Organ o = new Organ();
            o.setNazivOrgana(nazivOrgana);
            o.setPib(pib);
            o.setMaticniBroj(mp);
            o.setIdTipOrgana(new TipOrgana(idTipOrgan));
            os.addOrgan(o);
            porukaInsertOrgan = 1;
        } catch (Exception ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
            porukaInsertOrgan = 2;
        }

        return "redirect:/admin/korisnik";
    }
    
}
