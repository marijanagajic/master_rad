
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.Registar;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface RegistarDao {
    
    void addRegistar(Registar registar);
    void editRegistar(Registar registar);
    void deleteRegistar(int idRegistar);
    Registar findRegistar(int idRegistar);
    Registar findRegistarbyName(String nazivRegistar);
    List<Registar> findAllRegistar();
}
