
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface FrekventnostAzuriranjaRegistraDao {
    
    void addFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistra);
    void editFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistra);
    void deleteFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra);
    FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra);
    FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistrabyName(String nazivFrekventnostAzuriranjaRegistra);
    List<FrekventnostAzuriranjaRegistra> findAllFrekventnostAzuriranjaRegistra();
}
