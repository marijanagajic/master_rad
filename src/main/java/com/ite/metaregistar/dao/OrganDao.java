
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.Organ;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface OrganDao {
    
    void addOrgan(Organ organ);
    void editOrgan(Organ organ);
    void deleteOrgan(int idOrgan);
    Organ findOrgan(int idOrgan);
    Organ findOrganbyName(String nazivOrgan);
    List<Organ> findAllOrgane();
}
