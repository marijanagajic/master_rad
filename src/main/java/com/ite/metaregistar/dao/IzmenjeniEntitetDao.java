
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.IzmenjeniEntitet;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface IzmenjeniEntitetDao {
    
    void addIzmenjeniEntitet(IzmenjeniEntitet izmenjeniEntitet);
}
