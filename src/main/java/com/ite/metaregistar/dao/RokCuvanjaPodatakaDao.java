
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.RokCuvanjaPodataka;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface RokCuvanjaPodatakaDao {
    
    void addRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka);
    void editRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka);
    void deleteRokCuvanjaPodataka(int idRokCuvanjaPodataka);
    RokCuvanjaPodataka findRokCuvanjaPodataka(int idRokCuvanjaPodataka);
    RokCuvanjaPodataka findRokCuvanjaPodatakabyName(String nazivRokCuvanjaPodataka);
    List<RokCuvanjaPodataka> findAllRokCuvanjaPodataka();
}
