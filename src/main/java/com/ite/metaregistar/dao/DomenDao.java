
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.Domen;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface DomenDao {
    
    void addDomen(Domen domen);
    void editDomen(Domen domen);
    void deleteDomen(int idDomen);
    Domen findDomen(int idDomen);
    Domen findDomenbyName(String nazivDomen);
    List<Domen> findAllDomene();
}
