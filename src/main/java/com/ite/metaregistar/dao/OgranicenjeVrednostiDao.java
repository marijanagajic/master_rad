
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.OgranicenjeVrednosti;
import com.ite.metaregistar.model.OgranicenjeVrednostiPK;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface OgranicenjeVrednostiDao {
    
    void addOgranicenjeVrednosti(OgranicenjeVrednosti ogranicenjeVrednosti);
    void editOgranicenjeVrednosti(OgranicenjeVrednosti ogranicenjeVrednosti);
    OgranicenjeVrednosti findOgranicenjeVrednosti(OgranicenjeVrednostiPK ogranicenjeVrednostiPK);
    OgranicenjeVrednosti findOgranicenjeVrednostibyName(String nazivOgranicenjeVrednosti);
    List<OgranicenjeVrednosti> findAllOgranicenjeVrednostie();
}
