
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.VrstaOsobine;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface VrstaOsobineDao {
    
    void addVrstaOsobine(VrstaOsobine vrstaOsobine);
    void editVrstaOsobine(VrstaOsobine vrstaOsobine);
    void deleteVrstaOsobine(int idVrstaOsobine);
    VrstaOsobine findVrstaOsobine(int idVrstaOsobine);
    VrstaOsobine findVrstaOsobinebyName(String nazivVrstaOsobine);
    List<VrstaOsobine> findAllVrstaOsobinee();
}
