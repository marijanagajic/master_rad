
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.PravniOsnov;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface PravniOsnovDao {
    
    void addPravniOsnov(PravniOsnov pravniOsnov);
    void editPravniOsnov(PravniOsnov pravniOsnov);
    void deletePravniOsnov(int idPravniOsnov);
    PravniOsnov findPravniOsnov(int idPravniOsnov);
    PravniOsnov findPravniOsnovbyName(String nazivPravniOsnov);
    List<PravniOsnov> findAllPravniOsnove();
}
