package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.EntitetRegistraDao;
import com.ite.metaregistar.model.EntitetRegistra;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class EntitetRegistraDaoImpl implements EntitetRegistraDao {

    @Autowired
    private SessionFactory session;

    private EntitetRegistra entitetRegistra;

    @Override
    public void addEntitetRegistra(EntitetRegistra entitetRegistra) {
        session.getCurrentSession().save(entitetRegistra);
    }

    @Override
    public void editEntitetRegistra(EntitetRegistra entitetRegistra) {
        session.getCurrentSession().update(entitetRegistra);

    }

    @Override
    public void deleteEntitetRegistra(int idEntitetRegistra) {
        session.getCurrentSession().delete(findEntitetRegistra(idEntitetRegistra));

    }

    @Override
    public EntitetRegistra findEntitetRegistra(int idEntitetRegistra) {
        return session.getCurrentSession().get(EntitetRegistra.class, idEntitetRegistra);
    }

    @Override
    public EntitetRegistra findEntitetRegistrabyName(String nazivEntitetRegistra) {
        String hql = "FROM EntitetRegistra WHERE nazivEntitetRegistra=:nEntitetRegistra";
        Query query = session.getCurrentSession().createQuery(hql);
        query.setParameter("nEntitetRegistra", nazivEntitetRegistra);
        entitetRegistra = (EntitetRegistra) query.getSingleResult();
        return entitetRegistra;
    }

    @Override
    public List<EntitetRegistra> findAllEntitetRegistra() {
        return session.getCurrentSession().createQuery("FROM EntitetRegistra").list();
    }

    @Override
    public List<EntitetRegistra> findAllEntitetRegistraByIdRegistar(int idRegistar) {
        String hql = "FROM EntitetRegistra WHERE idRegistar="+String.valueOf(idRegistar);
        Query query = session.getCurrentSession().createQuery(hql);
        List<EntitetRegistra> lista = (List<EntitetRegistra>) query.getResultList();
        return lista;
    }

    @Override
    public List<EntitetRegistra> findAllEntitetRegistraNijeIzvorni() {
        String hql = "FROM EntitetRegistra WHERE izvorniRegistar=0";
        Query query = session.getCurrentSession().createQuery(hql);
        List<EntitetRegistra> lista = (List<EntitetRegistra>) query.getResultList();
        return lista;
    }

}
