
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.AtributDomenaDao;
import com.ite.metaregistar.model.AtributDomena;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class AtributDomenaDaoImpl implements AtributDomenaDao {
   
   @Autowired
   private SessionFactory session;
   
   private AtributDomena atributDomena;
   

    @Override
    public void addAtributDomena(AtributDomena atributDomena) {
        session.getCurrentSession().save(atributDomena);
    }

    @Override
    public void editAtributDomena(AtributDomena atributDomena) {
        session.getCurrentSession().update(atributDomena);

    }

    @Override
    public void deleteAtributDomena(int idAtributDomena) {
        session.getCurrentSession().delete(findAtributDomena(idAtributDomena));
      
    }

    @Override
    public AtributDomena findAtributDomena(int idAtributDomena) {
         return session.getCurrentSession().get(AtributDomena.class, idAtributDomena);
    }

    @Override
    public AtributDomena findAtributDomenabyName(String nazivAtributDomena) {
    String hql="FROM AtributDomena WHERE nazivAtributDomena=:nAtributDomena";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nAtributDomena", nazivAtributDomena);
    atributDomena=(AtributDomena)query.getSingleResult();
    return atributDomena;
    }

    @Override
    public List<AtributDomena> findAllAtributDomenae() {
      return session.getCurrentSession().createQuery("FROM AtributDomena").list();
    }
    
}
