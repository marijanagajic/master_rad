
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.IzmenjeniEntitetDao;
import com.ite.metaregistar.model.IzmenjeniEntitet;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class IzmenjeniEntitetDaoImpl implements IzmenjeniEntitetDao {
   
   @Autowired
   private SessionFactory session;
   

    @Override
    public void addIzmenjeniEntitet(IzmenjeniEntitet izmenjeniEntitet) {
        session.getCurrentSession().save(izmenjeniEntitet);
    }
    
}
