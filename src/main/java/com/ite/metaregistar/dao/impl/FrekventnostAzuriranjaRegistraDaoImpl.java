
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.FrekventnostAzuriranjaRegistraDao;
import com.ite.metaregistar.model.FrekventnostAzuriranjaRegistra;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class FrekventnostAzuriranjaRegistraDaoImpl implements FrekventnostAzuriranjaRegistraDao {
   
   @Autowired
   private SessionFactory session;
   
   private FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistraDaoImpl;
   

    @Override
    public void addFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistraDaoImpl) {
        session.getCurrentSession().save(frekventnostAzuriranjaRegistraDaoImpl);
    }

    @Override
    public void editFrekventnostAzuriranjaRegistra(FrekventnostAzuriranjaRegistra frekventnostAzuriranjaRegistraDaoImpl) {
        session.getCurrentSession().update(frekventnostAzuriranjaRegistraDaoImpl);

    }

    @Override
    public void deleteFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra) {
        session.getCurrentSession().delete(findFrekventnostAzuriranjaRegistra(idFrekventnostAzuriranjaRegistra));
      
    }

    @Override
    public FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistra(int idFrekventnostAzuriranjaRegistra) {
         return session.getCurrentSession().get(FrekventnostAzuriranjaRegistra.class, idFrekventnostAzuriranjaRegistra);
    }

    @Override
    public FrekventnostAzuriranjaRegistra findFrekventnostAzuriranjaRegistrabyName(String nazivFrekventnostAzuriranjaRegistra) {
    String hql="FROM FrekventnostAzuriranjaRegistra WHERE nazivFrekventnostAzuriranjaRegistra=:nFrekventnostAzuriranjaRegistra";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nFrekventnostAzuriranjaRegistra", nazivFrekventnostAzuriranjaRegistra);
    frekventnostAzuriranjaRegistraDaoImpl=(FrekventnostAzuriranjaRegistra)query.getSingleResult();
    return frekventnostAzuriranjaRegistraDaoImpl;
    }

    @Override
    public List<FrekventnostAzuriranjaRegistra> findAllFrekventnostAzuriranjaRegistra() {
      return session.getCurrentSession().createQuery("FROM FrekventnostAzuriranjaRegistra").list();
    }
    
}
