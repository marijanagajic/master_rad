
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.PravniOsnovDao;
import com.ite.metaregistar.model.PravniOsnov;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class PravniOsnovDaoImpl implements PravniOsnovDao {
   
   @Autowired
   private SessionFactory session;
   
   private PravniOsnov pravniOsnov;
   

    @Override
    public void addPravniOsnov(PravniOsnov pravniOsnov) {
        session.getCurrentSession().save(pravniOsnov);
    }

    @Override
    public void editPravniOsnov(PravniOsnov pravniOsnov) {
        session.getCurrentSession().update(pravniOsnov);

    }

    @Override
    public void deletePravniOsnov(int idPravniOsnov) {
        session.getCurrentSession().delete(findPravniOsnov(idPravniOsnov));
      
    }

    @Override
    public PravniOsnov findPravniOsnov(int idPravniOsnov) {
         return session.getCurrentSession().get(PravniOsnov.class, idPravniOsnov);
    }

    @Override
    public PravniOsnov findPravniOsnovbyName(String nazivPravniOsnov) {
    String hql="FROM PravniOsnov WHERE nazivPravniOsnov=:nPravniOsnov";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nPravniOsnov", nazivPravniOsnov);
    pravniOsnov=(PravniOsnov)query.getSingleResult();
    return pravniOsnov;
    }

    @Override
    public List<PravniOsnov> findAllPravniOsnove() {
      return session.getCurrentSession().createQuery("FROM PravniOsnov").list();
    }
    
}
