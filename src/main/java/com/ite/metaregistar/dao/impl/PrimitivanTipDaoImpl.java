
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.PrimitivanTipDao;
import com.ite.metaregistar.model.PrimitivanTip;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class PrimitivanTipDaoImpl implements PrimitivanTipDao {
   
   @Autowired
   private SessionFactory session;
   
   private PrimitivanTip primitivanTip;
   

    @Override
    public void addPrimitivanTip(PrimitivanTip primitivanTip) {
        session.getCurrentSession().save(primitivanTip);
    }

    @Override
    public void editPrimitivanTip(PrimitivanTip primitivanTip) {
        session.getCurrentSession().update(primitivanTip);

    }

    @Override
    public void deletePrimitivanTip(int idPrimitivanTip) {
        session.getCurrentSession().delete(findPrimitivanTip(idPrimitivanTip));
      
    }

    @Override
    public PrimitivanTip findPrimitivanTip(int idPrimitivanTip) {
         return session.getCurrentSession().get(PrimitivanTip.class, idPrimitivanTip);
    }

    @Override
    public PrimitivanTip findPrimitivanTipbyName(String nazivPrimitivanTip) {
    String hql="FROM PrimitivanTip WHERE nazivPrimitivanTip=:nPrimitivanTip";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nPrimitivanTip", nazivPrimitivanTip);
    primitivanTip=(PrimitivanTip)query.getSingleResult();
    return primitivanTip;
    }

    @Override
    public List<PrimitivanTip> findAllPrimitivanTipe() {
      return session.getCurrentSession().createQuery("FROM PrimitivanTip").list();
    }
    
}
