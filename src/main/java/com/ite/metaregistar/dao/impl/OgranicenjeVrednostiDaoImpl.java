
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.OgranicenjeVrednostiDao;
import com.ite.metaregistar.model.OgranicenjeVrednosti;
import com.ite.metaregistar.model.OgranicenjeVrednostiPK;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class OgranicenjeVrednostiDaoImpl implements OgranicenjeVrednostiDao {
   
   @Autowired
   private SessionFactory session;
   
   private OgranicenjeVrednosti ogranicenjeVrednosti;
   

    @Override
    public void addOgranicenjeVrednosti(OgranicenjeVrednosti ogranicenjeVrednosti) {
        session.getCurrentSession().save(ogranicenjeVrednosti);
    }

    @Override
    public void editOgranicenjeVrednosti(OgranicenjeVrednosti ogranicenjeVrednosti) {
        session.getCurrentSession().update(ogranicenjeVrednosti);

    }

    @Override
    public OgranicenjeVrednosti findOgranicenjeVrednosti(OgranicenjeVrednostiPK ogranicenjeVrednostiPK) {
         return session.getCurrentSession().get(OgranicenjeVrednosti.class, ogranicenjeVrednostiPK);
    }

    @Override
    public OgranicenjeVrednosti findOgranicenjeVrednostibyName(String nazivOgranicenjeVrednosti) {
    String hql="FROM OgranicenjeVrednosti WHERE nazivOgranicenjeVrednosti=:nOgranicenjeVrednosti";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nOgranicenjeVrednosti", nazivOgranicenjeVrednosti);
    ogranicenjeVrednosti=(OgranicenjeVrednosti)query.getSingleResult();
    return ogranicenjeVrednosti;
    }

    @Override
    public List<OgranicenjeVrednosti> findAllOgranicenjeVrednostie() {
      return session.getCurrentSession().createQuery("FROM OgranicenjeVrednosti").list();
    }
    
}
