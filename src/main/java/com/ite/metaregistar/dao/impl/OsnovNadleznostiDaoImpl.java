
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.OsnovNadleznostiDao;
import com.ite.metaregistar.model.OsnovNadleznosti;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class OsnovNadleznostiDaoImpl implements OsnovNadleznostiDao {
   
   @Autowired
   private SessionFactory session;
   
   private OsnovNadleznosti osnovNadleznosti;
   

    @Override
    public void addOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti) {
        session.getCurrentSession().save(osnovNadleznosti);
    }

    @Override
    public void editOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti) {
        session.getCurrentSession().update(osnovNadleznosti);

    }

    @Override
    public void deleteOsnovNadleznosti(int idOsnovNadleznosti) {
        session.getCurrentSession().delete(findOsnovNadleznosti(idOsnovNadleznosti));
      
    }

    @Override
    public OsnovNadleznosti findOsnovNadleznosti(int idOsnovNadleznosti) {
         return session.getCurrentSession().get(OsnovNadleznosti.class, idOsnovNadleznosti);
    }

    @Override
    public OsnovNadleznosti findOsnovNadleznostibyName(String nazivOsnovNadleznosti) {
    String hql="FROM OsnovNadleznosti WHERE nazivOsnovNadleznosti=:nOsnovNadleznosti";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nOsnovNadleznosti", nazivOsnovNadleznosti);
    osnovNadleznosti=(OsnovNadleznosti)query.getSingleResult();
    return osnovNadleznosti;
    }

    @Override
    public List<OsnovNadleznosti> findAllOsnovNadleznostie() {
      return session.getCurrentSession().createQuery("FROM OsnovNadleznosti").list();
    }
    
}
