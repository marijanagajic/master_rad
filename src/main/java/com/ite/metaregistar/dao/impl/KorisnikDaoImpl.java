/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.KorisnikDao;
import com.ite.metaregistar.model.Korisnik;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class KorisnikDaoImpl implements KorisnikDao {
    
    @Autowired
    private SessionFactory session;
    
    private Korisnik korisnik;
    
    @Override
    public void addKorisnik(Korisnik korisnik) {
        session.getCurrentSession().save(korisnik);
    }
    
    @Override
    public void editKorisnik(Korisnik korisnik) {
        session.getCurrentSession().update(korisnik);
    }
    
    @Override
    public void deleteKorisnik(int idKorisnik) {
        session.getCurrentSession().delete(findKorisnik(idKorisnik));
    }
    
    @Override
    public Korisnik findKorisnik(int idKorisnik) {
        return (Korisnik) session.getCurrentSession().get(Korisnik.class, idKorisnik);
    }
    
    @Override
    public Korisnik findKorisnikbyName(String username) {
        
        String hql = "FROM Korisnik WHERE username=:Kusername";
        Query query = session.getCurrentSession().createQuery(hql);
        query.setParameter("Kusername", username);
        List<Korisnik> lista = query.getResultList();
        if (lista.isEmpty() || lista == null) {
            return null;
        }
        return lista.get(0);
    }
    
    @Override
    public List<Korisnik> findAllKorisnike() {
        return session.getCurrentSession().createQuery("FROM Korisnik").list();
    }
    
}
