
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.RokCuvanjaPodatakaDao;
import com.ite.metaregistar.model.RokCuvanjaPodataka;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class RokCuvanjaPodatakaDaoImpl implements RokCuvanjaPodatakaDao {
   
   @Autowired
   private SessionFactory session;
   
   private RokCuvanjaPodataka rokCuvanjaPodataka;
   

    @Override
    public void addRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka) {
        session.getCurrentSession().save(rokCuvanjaPodataka);
    }

    @Override
    public void editRokCuvanjaPodataka(RokCuvanjaPodataka rokCuvanjaPodataka) {
        session.getCurrentSession().update(rokCuvanjaPodataka);

    }

    @Override
    public void deleteRokCuvanjaPodataka(int idRokCuvanjaPodataka) {
        session.getCurrentSession().delete(findRokCuvanjaPodataka(idRokCuvanjaPodataka));
      
    }

    @Override
    public RokCuvanjaPodataka findRokCuvanjaPodataka(int idRokCuvanjaPodataka) {
         return session.getCurrentSession().get(RokCuvanjaPodataka.class, idRokCuvanjaPodataka);
    }

    @Override
    public RokCuvanjaPodataka findRokCuvanjaPodatakabyName(String nazivRokCuvanjaPodataka) {
    String hql="FROM RokCuvanjaPodataka WHERE nazivRokCuvanjaPodataka=:nRokCuvanjaPodataka";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nRokCuvanjaPodataka", nazivRokCuvanjaPodataka);
    rokCuvanjaPodataka=(RokCuvanjaPodataka)query.getSingleResult();
    return rokCuvanjaPodataka;
    }

    @Override
    public List<RokCuvanjaPodataka> findAllRokCuvanjaPodataka() {
      return session.getCurrentSession().createQuery("FROM RokCuvanjaPodataka").list();
    }
    
}
