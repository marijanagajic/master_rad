
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.VrstaOsobineDao;
import com.ite.metaregistar.model.VrstaOsobine;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class VrstaOsobineDaoImpl implements VrstaOsobineDao {
   
   @Autowired
   private SessionFactory session;
   
   private VrstaOsobine vrstaOsobine;
   

    @Override
    public void addVrstaOsobine(VrstaOsobine vrstaOsobine) {
        session.getCurrentSession().save(vrstaOsobine);
    }

    @Override
    public void editVrstaOsobine(VrstaOsobine vrstaOsobine) {
        session.getCurrentSession().update(vrstaOsobine);

    }

    @Override
    public void deleteVrstaOsobine(int idVrstaOsobine) {
        session.getCurrentSession().delete(findVrstaOsobine(idVrstaOsobine));
      
    }

    @Override
    public VrstaOsobine findVrstaOsobine(int idVrstaOsobine) {
         return session.getCurrentSession().get(VrstaOsobine.class, idVrstaOsobine);
    }

    @Override
    public VrstaOsobine findVrstaOsobinebyName(String nazivVrstaOsobine) {
    String hql="FROM VrstaOsobine WHERE nazivVrstaOsobine=:nVrstaOsobine";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nVrstaOsobine", nazivVrstaOsobine);
    vrstaOsobine=(VrstaOsobine)query.getSingleResult();
    return vrstaOsobine;
    }

    @Override
    public List<VrstaOsobine> findAllVrstaOsobinee() {
      return session.getCurrentSession().createQuery("FROM VrstaOsobine").list();
    }
    
}
