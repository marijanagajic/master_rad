
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.NivoPristupaDao;
import com.ite.metaregistar.model.NivoPristupa;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class NivoPristupaDaoImpl implements NivoPristupaDao {
   
   @Autowired
   private SessionFactory session;
   
   private NivoPristupa nivoPristupa;
   

    @Override
    public void addNivoPristupa(NivoPristupa nivoPristupa) {
        session.getCurrentSession().save(nivoPristupa);
    }

    @Override
    public void editNivoPristupa(NivoPristupa nivoPristupa) {
        session.getCurrentSession().update(nivoPristupa);

    }

    @Override
    public void deleteNivoPristupa(int idNivoPristupa) {
        session.getCurrentSession().delete(findNivoPristupa(idNivoPristupa));
      
    }

    @Override
    public NivoPristupa findNivoPristupa(int idNivoPristupa) {
         return session.getCurrentSession().get(NivoPristupa.class, idNivoPristupa);
    }

    @Override
    public NivoPristupa findNazivPristupa(String nazivPristupa) {
    String hql="FROM NivoPristupa WHERE nazivNivoPristupa=:nPristupa";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nPristupa", nazivPristupa);
    nivoPristupa=(NivoPristupa)query.getSingleResult();
    return nivoPristupa;
    }

    @Override
    public List<NivoPristupa> listNivoPristupa() {
      return session.getCurrentSession().createQuery("FROM NivoPristupa").list();
    }
    
}
