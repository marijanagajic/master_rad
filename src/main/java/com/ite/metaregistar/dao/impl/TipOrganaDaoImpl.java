
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.TipOrganaDao;
import com.ite.metaregistar.model.TipOrgana;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class TipOrganaDaoImpl implements TipOrganaDao {
   
   @Autowired
   private SessionFactory session;
   
   private TipOrgana tipOrgana;
   

    @Override
    public void addTipOrgana(TipOrgana tipOrgana) {
        session.getCurrentSession().save(tipOrgana);
    }

    @Override
    public void editTipOrgana(TipOrgana tipOrgana) {
        session.getCurrentSession().update(tipOrgana);

    }

    @Override
    public void deleteTipOrgana(int idTipOrgana) {
        session.getCurrentSession().delete(findTipOrgana(idTipOrgana));
      
    }

    @Override
    public TipOrgana findTipOrgana(int idTipOrgana) {
         return session.getCurrentSession().get(TipOrgana.class, idTipOrgana);
    }

    @Override
    public TipOrgana findTipOrganabyName(String nazivTipOrgana) {
    String hql="FROM TipOrgana WHERE nazivTipOrgana=:nTipOrgana";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nTipOrgana", nazivTipOrgana);
    tipOrgana=(TipOrgana)query.getSingleResult();
    return tipOrgana;
    }

    @Override
    public List<TipOrgana> findAllTipOrgana() {
      return session.getCurrentSession().createQuery("FROM TipOrgana").list();
    }
    
}
