
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.OrganDao;
import com.ite.metaregistar.model.Organ;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class OrganDaoImpl implements OrganDao {
   
   @Autowired
   private SessionFactory session;
   
   private Organ organ;
   

    @Override
    public void addOrgan(Organ organ) {
        session.getCurrentSession().save(organ);
    }

    @Override
    public void editOrgan(Organ organ) {
        session.getCurrentSession().update(organ);

    }

    @Override
    public void deleteOrgan(int idOrgan) {
        session.getCurrentSession().delete(findOrgan(idOrgan));
      
    }

    @Override
    public Organ findOrgan(int idOrgan) {
         return session.getCurrentSession().get(Organ.class, idOrgan);
    }

    @Override
    public Organ findOrganbyName(String nazivOrgan) {
    String hql="FROM Organ WHERE nazivOrgan=:nOrgan";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nOrgan", nazivOrgan);
    organ=(Organ)query.getSingleResult();
    return organ;
    }

    @Override
    public List<Organ> findAllOrgane() {
      return session.getCurrentSession().createQuery("FROM Organ").list();
    }
    
}
