
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.RegistarDao;
import com.ite.metaregistar.model.Registar;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class RegistarDaoImpl implements RegistarDao {
   
   @Autowired
   private SessionFactory session;
   
   private Registar registar;
   

    @Override
    public void addRegistar(Registar registar) {
        session.getCurrentSession().save(registar);
    }

    @Override
    public void editRegistar(Registar registar) {
        session.getCurrentSession().update(registar);

    }

    @Override
    public void deleteRegistar(int idRegistar) {
        session.getCurrentSession().delete(findRegistar(idRegistar));
      
    }

    @Override
    public Registar findRegistar(int idRegistar) {
         return session.getCurrentSession().get(Registar.class, idRegistar);
    }

    @Override
    public Registar findRegistarbyName(String nazivRegistar) {
    String hql="FROM Registar WHERE nazivRegistar=:nRegistar";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nRegistar", nazivRegistar);
    registar=(Registar)query.getSingleResult();
    return registar;
    }

    @Override
    public List<Registar> findAllRegistar() {
      return session.getCurrentSession().createQuery("FROM Registar").list();
    }
    
}
