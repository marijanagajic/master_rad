
package com.ite.metaregistar.dao.impl;

import com.ite.metaregistar.dao.DomenDao;
import com.ite.metaregistar.model.Domen;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jelen
 */
@Repository
@Transactional
@EnableTransactionManagement
public class DomenDaoImpl implements DomenDao {
   
   @Autowired
   private SessionFactory session;
   
   private Domen domen;
   

    @Override
    public void addDomen(Domen domen) {
        session.getCurrentSession().save(domen);
    }

    @Override
    public void editDomen(Domen domen) {
        session.getCurrentSession().update(domen);

    }

    @Override
    public void deleteDomen(int idDomen) {
        session.getCurrentSession().delete(findDomen(idDomen));
      
    }

    @Override
    public Domen findDomen(int idDomen) {
         return session.getCurrentSession().get(Domen.class, idDomen);
    }

    @Override
    public Domen findDomenbyName(String nazivDomen) {
    String hql="FROM Domen WHERE nazivDomen=:nDomen";
    Query query=session.getCurrentSession().createQuery(hql);
    query.setParameter("nDomen", nazivDomen);
    domen=(Domen)query.getSingleResult();
    return domen;
    }

    @Override
    public List<Domen> findAllDomene() {
      return session.getCurrentSession().createQuery("FROM Domen").list();
    }
    
}
