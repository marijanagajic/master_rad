
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.PrimitivanTip;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface PrimitivanTipDao {
    
    void addPrimitivanTip(PrimitivanTip primitivanTip);
    void editPrimitivanTip(PrimitivanTip primitivanTip);
    void deletePrimitivanTip(int idPrimitivanTip);
    PrimitivanTip findPrimitivanTip(int idPrimitivanTip);
    PrimitivanTip findPrimitivanTipbyName(String nazivPrimitivanTip);
    List<PrimitivanTip> findAllPrimitivanTipe();
}
