
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.Korisnik;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface KorisnikDao {
    
    void addKorisnik(Korisnik korisnik);
    void editKorisnik(Korisnik korisnik);
    void deleteKorisnik(int idKorisnik);
    Korisnik findKorisnik(int idKorisnik);
    Korisnik findKorisnikbyName(String username);
    List<Korisnik> findAllKorisnike();
}
