
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.NivoPristupa;
import java.util.List;

/**
 *
 * @author jelen
 */
public interface NivoPristupaDao {
    
    void addNivoPristupa(NivoPristupa nivoPristupa);
    void editNivoPristupa(NivoPristupa nivoPristupa);
    void deleteNivoPristupa(int idNivoPristupa);
    NivoPristupa findNivoPristupa(int idNivoPristupa);
    NivoPristupa findNazivPristupa(String nazivPristupa);
    List<NivoPristupa> listNivoPristupa();
    
}
