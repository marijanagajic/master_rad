
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.EntitetRegistra;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface EntitetRegistraDao {
    
    void addEntitetRegistra(EntitetRegistra entitetRegistra);
    void editEntitetRegistra(EntitetRegistra entitetRegistra);
    void deleteEntitetRegistra(int idEntitetRegistra);
    EntitetRegistra findEntitetRegistra(int idEntitetRegistra);
    EntitetRegistra findEntitetRegistrabyName(String nazivEntitetRegistra);
    List<EntitetRegistra> findAllEntitetRegistra();
    List<EntitetRegistra> findAllEntitetRegistraNijeIzvorni();
    List<EntitetRegistra> findAllEntitetRegistraByIdRegistar(int idRegistar);
}
