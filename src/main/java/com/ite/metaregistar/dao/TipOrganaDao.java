
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.TipOrgana;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface TipOrganaDao {
    
    void addTipOrgana(TipOrgana tipOrgana);
    void editTipOrgana(TipOrgana tipOrgana);
    void deleteTipOrgana(int idTipOrgana);
    TipOrgana findTipOrgana(int idTipOrgana);
    TipOrgana findTipOrganabyName(String nazivTipOrgana);
    List<TipOrgana> findAllTipOrgana();
}
