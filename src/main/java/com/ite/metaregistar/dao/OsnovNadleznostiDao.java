
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.OsnovNadleznosti;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface OsnovNadleznostiDao {
    
    void addOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti);
    void editOsnovNadleznosti(OsnovNadleznosti osnovNadleznosti);
    void deleteOsnovNadleznosti(int idOsnovNadleznosti);
    OsnovNadleznosti findOsnovNadleznosti(int idOsnovNadleznosti);
    OsnovNadleznosti findOsnovNadleznostibyName(String nazivOsnovNadleznosti);
    List<OsnovNadleznosti> findAllOsnovNadleznostie();
}
