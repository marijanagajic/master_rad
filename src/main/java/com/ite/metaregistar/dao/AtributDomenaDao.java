
package com.ite.metaregistar.dao;
import com.ite.metaregistar.model.AtributDomena;
import java.util.List;
/**
 *
 * @author jelen
 */
public interface AtributDomenaDao {
    
    void addAtributDomena(AtributDomena atributDomena);
    void editAtributDomena(AtributDomena atributDomena);
    void deleteAtributDomena(int idAtributDomena);
    AtributDomena findAtributDomena(int idAtributDomena);
    AtributDomena findAtributDomenabyName(String nazivAtributDomena);
    List<AtributDomena> findAllAtributDomenae();
}
